package com.moviefinder.events;

public class ButtonClickEvent {

    private final int btnId;

    public ButtonClickEvent(int btnId) {
        this.btnId = btnId;
    }

    public int getBtnId() {
        return btnId;
    }
}