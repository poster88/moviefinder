package com.moviefinder.events;

public class FabSheetEvent {

    private final int viewId;

    public FabSheetEvent(int viewId) {
        this.viewId = viewId;
    }

    public int getViewId() {
        return viewId;
    }
}
