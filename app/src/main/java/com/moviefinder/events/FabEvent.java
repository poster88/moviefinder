package com.moviefinder.events;

public class FabEvent {

    private final boolean isVisible;

    public FabEvent(boolean isVisible) {
        this.isVisible = isVisible;
    }

    public boolean isVisible() {
        return isVisible;
    }
}
