package com.moviefinder.events;

public class BottomNavigationItemSelected {

    private final int event;

    public BottomNavigationItemSelected(int event) {
        this.event = event;
    }

    public int getEvent() {
        return event;
    }
}
