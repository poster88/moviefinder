package com.moviefinder.events;

public class MessageEvent {

    private final int resId;

    public MessageEvent(int resId) {
        this.resId = resId;
    }

    public int getResId() {
        return resId;
    }
}
