package com.moviefinder.events;

public class OffsetChangeEvent {

    private final int verticalOffset;

    public OffsetChangeEvent(int verticalOffset) {
        this.verticalOffset = verticalOffset;
    }

    public int getVerticalOffset() {
        return verticalOffset;
    }
}
