package com.moviefinder.events;

public class RetryConnectionsEvent {

    private final boolean isRetryConnection;

    public RetryConnectionsEvent(boolean isRetryConnection) {
        this.isRetryConnection = isRetryConnection;
    }

    public boolean isRetryConnection() {
        return isRetryConnection;
    }
}
