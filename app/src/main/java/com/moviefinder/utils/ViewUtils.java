package com.moviefinder.utils;

import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.widget.ImageView;

import com.moviefinder.R;
import com.squareup.picasso.Picasso;

public final class ViewUtils {

    public static float pxToDp(float px) {
        float densityDpi = Resources.getSystem().getDisplayMetrics().densityDpi;
        return px / (densityDpi / 160f);
    }

    public static int dpToPx(float dp) {
        float density = Resources.getSystem().getDisplayMetrics().density;
        return Math.round(dp * density);
    }

    public static void addFragment(@NonNull FragmentManager fm, int containerId, Fragment fragment, String tag) {
        fm.beginTransaction()
                .add(containerId, fragment, tag)
                .commit();
    }

    public static void replaceFragment(@NonNull FragmentManager fm, Fragment fragment, String tag) {
        fm.beginTransaction()
                .replace(R.id.search_view_placeholder, fragment)
                .addToBackStack(tag)
                .commit();
    }

    public static void removeFragment(@NonNull FragmentManager fm, Fragment fragment) {
        fm.beginTransaction()
                .remove(fragment)
                .commit();
    }

    public static void loadImage(final ImageView image, final String imagePath) {
        if (imagePath != null) {
            String ulr = AppConstants.BASE_IMAGE_PATH + imagePath;
            Picasso.with(image.getContext())
                    .load(ulr)
                    .placeholder(R.drawable.ic_placeholder_gray_56dp)
                    .into(image);
        }
    }

    public static void loadImage(final ImageView image, final String imagePath, int holderId) {
        if (imagePath != null) {
            String ulr = AppConstants.BASE_IMAGE_PATH + imagePath;
            Picasso.with(image.getContext())
                    .load(ulr)
                    .placeholder(holderId)
                    .into(image);
        }
    }
}