package com.moviefinder.utils;

import android.content.Context;
import android.content.res.AssetManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.moviefinder.R;
import com.moviefinder.data.model.content.Genre;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;

public final class CommonUtils {

    public static String loadJSONFromAsset(Context context, String jsonFileName)
            throws IOException {

        AssetManager manager = context.getAssets();
        InputStream is = manager.open(jsonFileName);

        int size = is.available();
        byte[] buffer = new byte[size];
        is.read(buffer);
        is.close();

        return new String(buffer, "UTF-8");
    }

    public static String getTextFromList(List<String> list) {
        return list.toString()
                .replace("[", "")
                .replace("]", "");
    }

    public static Float formatVoteAverage(double rating) {
        return ((float) rating) / 2;
    }

    public static String formatQuery(String query) {
        return query.trim().toLowerCase();
    }

    public static String formatDate(String date) {
        if (date != null) date = String.format("%.4s", date);
        return date;
    }

    public static String getStringAverage(double value) {
        return String.format("%.1f", value);
    }

    public static String getStringGenres(List<Integer> genreIds, Context context) {
        return null == genreIds ? null : getGenres(genreIds, context);
    }

    private static String getGenres(List<Integer> genreIds, Context context) {
        return Observable.fromIterable(genreIds)
                .onErrorReturn(throwable -> 0)
                .flatMap((Function<Integer, ObservableSource<String>>) genreId ->
                        Observable.fromIterable(getGenresList(context))
                                .filter(genre -> genre.getId() == genreId)
                                .map(Genre::getName))
                .toList()
                .map(CommonUtils::getTextFromList)
                .toObservable()
                .blockingFirst();
    }

    private static List<Genre> getGenresList(Context context) {
        List<Genre> genres = null;
        Type type = new TypeToken<List<Genre>>() {}.getType();
        try {
            genres = new Gson().fromJson(CommonUtils.loadJSONFromAsset(
                    context, AppConstants.GENRES_FILENAME), type);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return genres;
    }

    public static String getFormattedTitle(String title, String date, Context context) {
        return null == title || null == date ? null
                : String.format(context.getString(R.string.movie_info_text_title), title, date);
    }
}