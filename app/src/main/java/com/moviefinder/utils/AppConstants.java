package com.moviefinder.utils;

public final class AppConstants {

    public static final String BASE_IMAGE_PATH = "https://image.tmdb.org/t/p/w500/";
    public static final String GENRES_FILENAME = "seed/genres.json";
    public static final int IMAGE_WIDTH = 108;
    public static final int IMAGE_HEIGHT = 162;
    public static final int FADE_DIM_BACKGROUND_START = 0;
    public static final int FADE_DIM_BACKGROUND_END = 150;
    public static final int ANIMATION_DURATION = 250;
    public static final int SEARCH_DELAY = 700;
    public static final int MAX_SUGGESTION_COUNT = 5;
    public static final int DEFAULT_PAGE = 1;
    public static final int DEFAULT_SCROLL_POSITION = 0;
    public static final int STATE_DEFAULT = 0;
    public static final int STATE_IS_LOADING = 1;
    public static final int STATE_SHOW = 2;
    public static final int STATE_ERROR = -1;
    public static final int RESPONSE_ITEM_COUNT = 20;
    public static final String TYPE_TV_SHOW = "tv";
    public static final String TYPE_MOVIE = "movie";
    public static final String TYPE_PERSON = "person";
    public static final String TYPE_SEARCH_RESULT = "search";
}