package com.moviefinder;


import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

import com.moviefinder.data.repository.MovieRepository;
import com.moviefinder.data.repository.MovieRepositoryImpl;

public class MovieApp extends Application {

    @SuppressLint("StaticFieldLeak")
    private static Context context;
    private static MovieRepository movieRepository;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
    }

    @NonNull
    public static MovieRepository provideMovieRepository() {
        if (movieRepository == null) {
            movieRepository = new MovieRepositoryImpl(context);
        }
        return movieRepository;
    }
}