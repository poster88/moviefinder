package com.moviefinder.data.api;


import com.moviefinder.data.model.content.Movie;
import com.moviefinder.data.model.responce.MoviesResponse;
import com.moviefinder.data.model.responce.SearchResponse;
import com.moviefinder.data.model.content.TVShow;
import com.moviefinder.data.model.responce.TVShowsResponse;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

public interface MovieService {

    @GET("discover/movie")
    Observable<MoviesResponse> getMovies(
            @QueryMap Map<String, Object> queryMap
    );

    @GET("discover/tv")
    Observable<TVShowsResponse> getTVShows(
            @QueryMap Map<String, Object> queryMap
    );

    @GET("movie/{movie_id}")
    Observable<Movie> getMovie(
            @Path("movie_id") int movieId,
            @QueryMap Map<String, Object> queryMap
    );

    @GET("tv/{tv_id}")
    Observable<TVShow> getTVShow(
            @Path("tv_id") int tvId,
            @QueryMap Map<String, Object> queryMap
    );

    @GET("movie/top_rated")
    Observable<MoviesResponse> getMovieTopRated(
            @QueryMap Map<String, Object> queryMap
    );

    @GET("tv/top_rated")
    Observable<TVShowsResponse> getTVShowsTopRated(
            @QueryMap Map<String, Object> queryMap
    );

    @GET("movie/upcoming")
    Observable<MoviesResponse> getMovieUpcoming(
            @QueryMap Map<String, Object> queryMap
    );

    @GET("tv/airing_today")
    Observable<TVShowsResponse> getTVShowsAiringToday(
            @QueryMap Map<String, Object> queryMap
    );

    @GET("movie/now_playing")
    Observable<MoviesResponse> getMovieNowPlaying(
            @QueryMap Map<String, Object> queryMap
    );

    @GET("tv/on_the_air")
    Observable<TVShowsResponse> getTVShowsOnTheAir(
            @QueryMap Map<String, Object> queryMap
    );

    @GET("search/multi")
    Observable<SearchResponse> multiSearch(
            @QueryMap Map<String, Object> queryMap
    );
}