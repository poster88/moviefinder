package com.moviefinder.data.api;


import android.support.annotation.NonNull;

import com.moviefinder.BuildConfig;
import com.moviefinder.utils.RxErrorHandlingCallAdapterFactory;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public final class ApiFactory {

    private static OkHttpClient client;
    private static MovieService service;

    @NonNull
    public static MovieService getMovieService() {
        MovieService movieService = service;
        if (service == null) {
            synchronized (ApiFactory.class) {
                movieService = service;
                if (movieService == null) {
                    movieService = service = createService();
                }
            }
        }
        return  movieService;
    }

    @NonNull
    private static MovieService createService() {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(getClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
                .build()
                .create(MovieService.class);
    }

    @NonNull
    private static OkHttpClient getClient() {
        OkHttpClient okHttpClient = client;
        if (client == null) {
            synchronized (ApiFactory.class) {
                okHttpClient = client;
                if (okHttpClient == null) {
                    okHttpClient = client = buildClient();
                }
            }
        }
        return okHttpClient;
    }

    @NonNull
    private static OkHttpClient buildClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(new ApiKeyInterceptor())
                .build();
    }
}