package com.moviefinder.data.mapper;


import com.moviefinder.data.model.content.Movie;
import com.moviefinder.data.model.content.MultiSearch;
import com.moviefinder.data.model.content.TVShow;
import com.moviefinder.utils.AppConstants;

public class MovieMapper {

    public static MovieDTO mapMovie(Movie movie) {
        MovieDTO movieDTO = new MovieDTO();
        movieDTO.setId(movie.getId());
        movieDTO.setTitle(movie.getTitle());
        movieDTO.setBackdropPath(movie.getBackdropPath());
        movieDTO.setDate(movie.getReleaseDate());
        movieDTO.setGenreIds(movie.getGenreIds());
        movieDTO.setOverview(movie.getOverview());
        movieDTO.setPopularity(movie.getPopularity());
        movieDTO.setPosterPath(movie.getPosterPath());
        movieDTO.setVoteAverage(movie.getVoteAverage());
        movieDTO.setVoteCount(movie.getVoteCount());
        movieDTO.setMediaType(AppConstants.TYPE_MOVIE);
        return movieDTO;
    }

    public static MovieDTO mapTVShow(TVShow tvShow) {
        MovieDTO movieDTO = new MovieDTO();
        movieDTO.setId(tvShow.getId());
        movieDTO.setVoteCount(tvShow.getVoteCount());
        movieDTO.setOverview(tvShow.getOverview());
        movieDTO.setVoteAverage(tvShow.getVoteAverage());
        movieDTO.setTitle(tvShow.getName());
        movieDTO.setPopularity(tvShow.getPopularity());
        movieDTO.setPosterPath(tvShow.getPosterPath());
        movieDTO.setBackdropPath(tvShow.getBackdropPath());
        movieDTO.setGenreIds(tvShow.getGenres());
        movieDTO.setDate(tvShow.getFirstAirDate());
        movieDTO.setMediaType(AppConstants.TYPE_TV_SHOW);
        return movieDTO;
    }

    public static MovieDTO mapSearchResult(MultiSearch multiSearch) {
        MovieDTO movieDTO = new MovieDTO();
        movieDTO.setId(multiSearch.getId());
        movieDTO.setBackdropPath(multiSearch.getBackdropPath());
        movieDTO.setGenreIds(multiSearch.getGenreIds());
        movieDTO.setOverview(multiSearch.getOverview());
        movieDTO.setPopularity(multiSearch.getPopularity());
        movieDTO.setPosterPath(multiSearch.getPosterPath());
        movieDTO.setVoteAverage(multiSearch.getVoteAverage());
        movieDTO.setVoteCount(multiSearch.getVoteCount());
        movieDTO.setMediaType(multiSearch.getMediaType());
        boolean isTypeMovie = multiSearch.getMediaType().equals(AppConstants.TYPE_MOVIE);
        movieDTO.setTitle(isTypeMovie ? multiSearch.getTitle() : multiSearch.getName());
        movieDTO.setDate(isTypeMovie ? multiSearch.getReleaseDate() : multiSearch.getFirstAirDate());
        return movieDTO;
    }
}
