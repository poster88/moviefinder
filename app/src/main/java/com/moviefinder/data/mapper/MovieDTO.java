package com.moviefinder.data.mapper;

import java.util.List;

public class MovieDTO {
    private int id;
    private String mediaType;
    private double voteAverage;
    private float formattedVoteAverage;
    private String stringVoteAverage;
    private double popularity;
    private int voteCount;
    private String posterPath;
    private String backdropPath;
    private String title;
    private String formattedTitle;
    private String overview;
    private List<Integer> genreIds;
    private String stringGenres;
    private String date;
    private String formattedDate;
    private boolean isFavorite;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public double getVoteAverage() {
        return voteAverage;
    }

    public void setVoteAverage(double voteAverage) {
        this.voteAverage = voteAverage;
    }

    public float getFormattedVoteAverage() {
        return formattedVoteAverage;
    }

    public void setFormattedVoteAverage(float formattedVoteAverage) {
        this.formattedVoteAverage = formattedVoteAverage;
    }

    public String getStringVoteAverage() {
        return stringVoteAverage;
    }

    public void setStringVoteAverage(String stringVoteAverage) {
        this.stringVoteAverage = stringVoteAverage;
    }

    public double getPopularity() {
        return popularity;
    }

    public void setPopularity(double popularity) {
        this.popularity = popularity;
    }

    public int getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(int voteCount) {
        this.voteCount = voteCount;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public String getBackdropPath() {
        return backdropPath;
    }

    public void setBackdropPath(String backdropPath) {
        this.backdropPath = backdropPath;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFormattedTitle() {
        return formattedTitle;
    }

    public void setFormattedTitle(String formattedTitle) {
        this.formattedTitle = formattedTitle;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public List<Integer> getGenreIds() {
        return genreIds;
    }

    public void setGenreIds(List<Integer> genreIds) {
        this.genreIds = genreIds;
    }

    public String getStringGenres() {
        return stringGenres;
    }

    public void setStringGenres(String stringGenres) {
        this.stringGenres = stringGenres;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFormattedDate() {
        return formattedDate;
    }

    public void setFormattedDate(String formattedDate) {
        this.formattedDate = formattedDate;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    @Override
    public String toString() {
        return "MovieDTO{" +
                "id=" + id +
                ", mediaType='" + mediaType + '\'' +
                ", voteAverage=" + voteAverage +
                ", formattedVoteAverage=" + formattedVoteAverage +
                ", stringVoteAverage='" + stringVoteAverage + '\'' +
                ", popularity=" + popularity +
                ", voteCount=" + voteCount +
                ", posterPath='" + posterPath + '\'' +
                ", backdropPath='" + backdropPath + '\'' +
                ", title='" + title + '\'' +
                ", formattedTitle='" + formattedTitle + '\'' +
                ", overview='" + overview + '\'' +
                ", genreIds=" + genreIds +
                ", stringGenres='" + stringGenres + '\'' +
                ", date='" + date + '\'' +
                ", formattedDate='" + formattedDate + '\'' +
                ", isFavorite=" + isFavorite +
                '}';
    }
}
