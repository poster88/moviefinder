package com.moviefinder.data.repository;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.LruCache;

import com.google.gson.Gson;
import com.moviefinder.BuildConfig;
import com.moviefinder.R;
import com.moviefinder.data.api.ApiFactory;
import com.moviefinder.data.mapper.MovieDTO;
import com.moviefinder.data.mapper.MovieMapper;
import com.moviefinder.data.model.content.Movie;
import com.moviefinder.data.model.content.MultiSearch;
import com.moviefinder.data.model.content.TVShow;
import com.moviefinder.data.model.responce.MoviesResponse;
import com.moviefinder.data.model.responce.SearchResponse;
import com.moviefinder.data.model.responce.TVShowsResponse;
import com.moviefinder.ui.screens.search.QuerySuggestion;
import com.moviefinder.utils.AppConstants;
import com.moviefinder.utils.CommonUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;

public class MovieRepositoryImpl implements MovieRepository {

    public static final String TAG = "MovieRepositoryImpl";

    private final String LANGUAGE_KEY = "language";
    private final String SORT_BY_KEY = "sort_by";
    private final String POPULARITY_DESC_KEY = "popularity.desc";
    private final String PAGE_KEY = "page";
    private final String PREF_SEARCH_HISTORY = "search_history";
    private final String PREF_FAVORITE_ITEMS = "favorite_items";
    private final String QUERY_KEY = "query";
    private final int tvShowsCacheSize = 4;
    private final int filmsCacheSize = 4;
    private final int searchCacheSize = 50;

    private LruCache<Integer, List<MovieDTO>> tvShowsCache = new LruCache<>(tvShowsCacheSize);
    private LruCache<Integer, List<MovieDTO>> filmsCache = new LruCache<>(filmsCacheSize);
    private LruCache<String, List<MovieDTO>> searchCache = new LruCache<>(searchCacheSize);
    private SharedPreferences prefsFavorite;
    private SharedPreferences prefsHistory;
    private Context context;
    private int movieCategoryId;
    private int tVShowCategoryId;
    private int tvShowId;
    private int movieId;
    private int searchItemId;
    private String searchQuery;


    public MovieRepositoryImpl(Context context) {
        this.context = context;
        prefsFavorite = context.getSharedPreferences(PREF_FAVORITE_ITEMS, Context.MODE_PRIVATE);
        prefsHistory = context.getSharedPreferences(PREF_SEARCH_HISTORY, Context.MODE_PRIVATE);
    }

    @Override
    public Observable<List<MovieDTO>> getFilms(int page, int categoryId, int filterId) {
        return Observable.concat(getCacheFilms(categoryId), getNetWorkFilms(page, categoryId))
                .filter(movies -> !movies.isEmpty())
                .first(Collections.emptyList())
                .flatMapObservable(movies -> filterMovie(getGenre(filterId), movies));
    }

    @Override
    public Observable<List<MovieDTO>> getTVShows(int page, int categoryId, int filterId) {
        return Observable.concat(getCachedTVShows(categoryId), getNetWorkTVShows(page, categoryId))
                .filter(tvShows -> !tvShows.isEmpty())
                .first(Collections.emptyList())
                .flatMapObservable(tvShows -> filterMovie(getGenre(filterId), tvShows));
    }

    @Override
    public Observable<List<MovieDTO>> getSearchResults(int page, String query) {
        return Observable.concat(getCachedSearch(query), getNetWorkSearch(page, query))
                .filter(searches -> !searches.isEmpty())
                .first(Collections.emptyList())
                .toObservable();
    }

    private Observable<List<MovieDTO>> getNetWorkSearch(int page, String query) {
        return ApiFactory.getMovieService().multiSearch(getSearchQueryMap(page, query))
                .flatMap((Function<SearchResponse, ObservableSource<List<MovieDTO>>>) response ->
                        Observable.fromIterable(response.getResults())
                                .map(this::getMappedSearchResult)
                                .toList()
                                .doAfterSuccess(movieDTOList -> {
                                    saveSearchCache(query, movieDTOList);
                                    saveSuggestion(query);
                                })
                                .toObservable());
    }

    private Observable<List<MovieDTO>> getNetWorkFilms(int page, int categoryId) {
        return getFilmsByCategory(page, categoryId)
                .flatMap((Function<MoviesResponse, ObservableSource<List<MovieDTO>>>) response ->
                        Observable.fromIterable(response.getMovies())
                                .map(this::getMappedMovie)
                                .toList()
                                .doAfterSuccess(movieDetails -> saveFilmsCache(categoryId, movieDetails))
                                .toObservable());
    }

    private Observable<List<MovieDTO>> getNetWorkTVShows(int page, int categoryId) {
        return getTVShowsByCategory(page, categoryId)
                .flatMap((Function<TVShowsResponse, ObservableSource<List<MovieDTO>>>) response ->
                        Observable.fromIterable(response.getTvShows())
                                .map(this::getMappedMovie)
                                .toList()
                                .doAfterSuccess(tvShows -> saveTVShowsCache(categoryId, tvShows))
                                .toObservable());
    }

    @Override
    public Observable<List<MovieDTO>> getMoreFilms(int page, int categoryId, int filterId) {
        return getNetWorkFilms(page, categoryId)
                .flatMap((Function<List<MovieDTO>, ObservableSource<List<MovieDTO>>>) movies ->
                        filterMovie(getGenre(filterId), movies));
    }

    @Override
    public Observable<List<MovieDTO>> getMoreTVShows(int page, int categoryId, int filterId) {
        return getNetWorkTVShows(page, categoryId)
                .flatMap((Function<List<MovieDTO>, ObservableSource<List<MovieDTO>>>) tvShows ->
                        filterMovie(getGenre(filterId), tvShows));
    }

    private Observable<List<MovieDTO>> filterMovie(String genre, List<MovieDTO> movies) {
        return Observable.fromIterable(movies)
                .filter(movie -> movie.getStringGenres().contains(genre))
                .toList()
                .toObservable();
    }

    @Override
    public Observable<List<MovieDTO>> getMoreSearchResults(int page, String query) {
        return getNetWorkSearch(page, query);
    }

    private String getGenre(int viewId) {
        String genre;
        if (viewId == R.id.fab_sheet_genre_action) {
            genre = context.getString(R.string.genre_action);
        } else if (viewId == R.id.fab_sheet_genre_western) {
            genre = context.getString(R.string.genre_western);
        } else if (viewId == R.id.fab_sheet_genre_comedy) {
            genre = context.getString(R.string.genre_comedy);
        } else if (viewId == R.id.fab_sheet_genre_criminal) {
            genre = context.getString(R.string.genre_criminal);
        } else {
            genre = "";
        }
        return genre;
    }

    private void saveFilmsCache(int categoryId, List<MovieDTO> movies) {
        if (null == filmsCache.get(categoryId)) {
            filmsCache.put(categoryId, movies);
        } else {
            filmsCache.get(categoryId).addAll(movies);
        }
    }

    private void saveTVShowsCache(int categoryId, List<MovieDTO> tvShows) {
        if (null == tvShowsCache.get(categoryId)) {
            tvShowsCache.put(categoryId, tvShows);
        } else {
            tvShowsCache.get(categoryId).addAll(tvShows);
        }
    }

    private void saveSearchCache(String key, List<MovieDTO> searchResults) {
        if (null == searchCache.get(key)) {
            searchCache.put(key, searchResults);
        } else {
            searchCache.get(key).addAll(searchResults);
        }
    }

    private Observable<List<MovieDTO>> getCacheFilms(int categoryId) {
        return Observable.just(null == filmsCache.get(categoryId)
                ? new ArrayList<MovieDTO>() : filmsCache.get(categoryId));
    }

    private Observable<List<MovieDTO>> getCachedTVShows(int categoryId) {
        return Observable.just(null == tvShowsCache.get(categoryId)
                ? new ArrayList<MovieDTO>() : tvShowsCache.get(categoryId));
    }

    private Observable<List<MovieDTO>> getCachedSearch(String query) {
        return Observable.just(null == searchCache.get(query)
                ? new ArrayList<MovieDTO>() : searchCache.get(query));
    }

    public Observable<MoviesResponse> getFilmsByCategory(int page, int categoryId) {
        if (categoryId == R.id.action_actual) {
            return ApiFactory.getMovieService().getMovies(getMoviesQueryMap(page));
        } else if (categoryId == R.id.action_rated) {
            return ApiFactory.getMovieService().getMovieTopRated(getMoviesQueryMap(page));
        } else if (categoryId == R.id.action_in_release) {
            return ApiFactory.getMovieService().getMovieUpcoming(getMoviesQueryMap(page));
        } else {
            return ApiFactory.getMovieService().getMovieNowPlaying(getMoviesQueryMap(page));
        }
    }

    private Observable<TVShowsResponse> getTVShowsByCategory(int page, int categoryId) {
        if (categoryId == R.id.action_actual) {
            return ApiFactory.getMovieService().getTVShows(getTVShowsQueryMap(page));
        } else if (categoryId == R.id.action_rated) {
            return ApiFactory.getMovieService().getTVShowsTopRated(getTVShowsQueryMap(page));
        } else if (categoryId == R.id.action_in_release) {
            return ApiFactory.getMovieService().getTVShowsAiringToday(getTVShowsQueryMap(page));
        } else {
            return ApiFactory.getMovieService().getTVShowsOnTheAir(getTVShowsQueryMap(page));
        }
    }

    private MovieDTO getMappedMovie(TVShow tvShow) {
        MovieDTO movieDTO = MovieMapper.mapTVShow(tvShow);
        return formatMovie(movieDTO);
    }

    private MovieDTO getMappedMovie(Movie film) {
        MovieDTO movieDTO = MovieMapper.mapMovie(film);
        return formatMovie(movieDTO);
    }

    private MovieDTO getMappedSearchResult(MultiSearch multiSearch) {
        MovieDTO movieDTO = MovieMapper.mapSearchResult(multiSearch);
        return formatMovie(movieDTO);
    }

    private MovieDTO formatMovie(MovieDTO movieDTO) {
        movieDTO.setFormattedDate(CommonUtils.formatDate(movieDTO.getDate()));
        movieDTO.setFormattedVoteAverage(CommonUtils.formatVoteAverage(movieDTO.getVoteAverage()));
        movieDTO.setStringVoteAverage(CommonUtils.getStringAverage(movieDTO.getVoteAverage()));
        movieDTO.setStringGenres(CommonUtils.getStringGenres(movieDTO.getGenreIds(), context));
        movieDTO.setFormattedTitle(CommonUtils.getFormattedTitle(movieDTO.getTitle(), movieDTO.getFormattedDate(), context));
        return movieDTO;
    }

    private Map<String, Object> getMoviesQueryMap(int page) {
        Map<String, Object> map = new HashMap<>();
        map.put(LANGUAGE_KEY, BuildConfig.BASE_LANGUAGE);
        map.put(SORT_BY_KEY, POPULARITY_DESC_KEY);
        map.put(PAGE_KEY, page);
        return map;
    }

    private Map<String, Object> getTVShowsQueryMap(int page) {
        Map<String, Object> map = new HashMap<>();
        map.put(LANGUAGE_KEY, BuildConfig.BASE_LANGUAGE);
        map.put(SORT_BY_KEY, POPULARITY_DESC_KEY);
        map.put(PAGE_KEY, page);
        return map;
    }

    private Map<String, Object> getSearchQueryMap(int page, String query) {
        Map<String, Object> map = new HashMap<>();
        map.put(LANGUAGE_KEY, BuildConfig.BASE_LANGUAGE);
        map.put(QUERY_KEY, query);
        map.put(PAGE_KEY, page);
        return map;
    }

    @Override
    public Observable<List<QuerySuggestion>> findSuggestions(String query) {
        return Observable.fromIterable((prefsHistory.getAll().values()))
                .map((Function<Object, QuerySuggestion>) o -> getSuggestionFromJson((String) o))
                .filter(suggestion -> suggestion.getBody().contains(query))
                .toList()
                .map(this::getMaxSuggestions)
                .toObservable();
    }

    private void saveSuggestion(String query) {
        String jsonModel = new Gson().toJson(new QuerySuggestion(query, true));
        prefsHistory.edit().putString(query, jsonModel).apply();
    }

    private QuerySuggestion getSuggestionFromJson(String s) {
        return new Gson().fromJson(s, QuerySuggestion.class);
    }

    private List<QuerySuggestion> getMaxSuggestions(List<QuerySuggestion> querySuggestions) {
        if (querySuggestions.size() > AppConstants.MAX_SUGGESTION_COUNT) {
            int from = querySuggestions.size() - AppConstants.MAX_SUGGESTION_COUNT + 1;
            int to = querySuggestions.size() - 1;
            querySuggestions = querySuggestions.subList(from, to);
        }
        return querySuggestions;
    }

    @Override
    public void setLastMovieDetails(int categoryId, int lastMovieId) {
        movieCategoryId = categoryId;
        movieId = lastMovieId;
    }

    @Override
    public void setLastTVShowDetails(int categoryId, int lastTVShowId) {
        tVShowCategoryId = categoryId;
        tvShowId = lastTVShowId;
    }

    @Override
    public void setLastSearchDetails(String query, int itemId) {
        searchItemId = itemId;
        searchQuery = query;
    }

    @Override
    public Observable<List<MovieDTO>> getPlayListItems() {
        return Observable.fromIterable(prefsFavorite.getAll().values())
                .map(item -> new Gson().fromJson((String) item, MovieDTO.class))
                .toSortedList((o1, o2) -> o1.getTitle().compareTo(o2.getTitle()))
                .toObservable();
    }

    private String getJsonModel(String type) {
        MovieDTO movieDTO;
        if (type.equals(AppConstants.TYPE_MOVIE)) {
            movieDTO = getMovieInfo();
        } else if (type.equals(AppConstants.TYPE_TV_SHOW)) {
            movieDTO = getTVShowsInfo();
        } else {
            movieDTO = getSearchInfo();
        }
        return new Gson().toJson(movieDTO, MovieDTO.class);
    }

    private void addItemToSharedPref(String key, String value) {
        prefsFavorite.edit()
                .putString(key, value)
                .apply();
    }

    @Override
    public Completable removeItemFromFavorite(String itemId) {
        return Completable.fromCallable((Callable<Boolean>) prefsFavorite.edit()
                .remove(itemId)::commit);
    }

    @Override
    public Completable removeAllItemsFromFavorite() {
        return Completable.fromCallable((Callable<Boolean>) prefsFavorite.edit()
                .clear()::commit);
    }

    @Override
    public Observable<Boolean> addToFavorites(String itemId, String type) {
        return Observable.just(!prefsFavorite.contains(itemId))
                .doOnNext(isAddToFavorite -> {
                    if (isAddToFavorite) addItemToSharedPref(itemId, getJsonModel(type));
                });
    }

    @Override
    public MovieDTO getMovieInfo() {
        MovieDTO movieDTO = null;
        List<MovieDTO> movies = filmsCache.get(movieCategoryId);
        if (movies != null) {
            movieDTO = initInfo(movies, movieId);
        }
        if (movieDTO == null) {
            movieDTO = new Gson().fromJson(prefsFavorite.getString(String.valueOf(movieId), null), MovieDTO.class);
        }
        return movieDTO;
    }

    @Override
    public MovieDTO getTVShowsInfo() {
        MovieDTO movieDTO = null;
        List<MovieDTO> tvShows = tvShowsCache.get(tVShowCategoryId);
        if (tvShows != null) {
            movieDTO = initInfo(tvShows, tvShowId);
        }
        if (movieDTO == null) {
            movieDTO = new Gson().fromJson(prefsFavorite.getString(String.valueOf(tvShowId), null), MovieDTO.class);
        }
        return movieDTO;
    }

    @Override
    public MovieDTO getSearchInfo() {
        List<MovieDTO> searchResults = searchCache.get(searchQuery);
        return null == searchResults ? null : initInfo(searchResults, searchItemId);
    }

    private MovieDTO initInfo(List<MovieDTO> dataList, int searchItemId) {
        MovieDTO movieDTO = null;
        for (MovieDTO model : dataList) {
            int itemId = model.getId();
            if (itemId == searchItemId) {
                boolean isInFavorite = prefsFavorite.contains(String.valueOf(itemId));
                model.setFavorite(isInFavorite);
                movieDTO = model;
                break;
            }
        }
        return movieDTO;
    }
}