package com.moviefinder.data.repository;


import com.moviefinder.data.mapper.MovieDTO;
import com.moviefinder.data.model.responce.MoviesResponse;
import com.moviefinder.ui.screens.search.QuerySuggestion;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;

public interface MovieRepository {

    Observable<List<MovieDTO>> getFilms(int page, int categoryId, int filterId);

    Observable<List<MovieDTO>> getTVShows(int page, int categoryId, int filterId);

    Observable<List<MovieDTO>> getMoreFilms(int page, int categoryId, int filterId);

    Observable<List<MovieDTO>> getMoreTVShows(int page, int categoryId, int filterId);

    Observable<List<MovieDTO>> getSearchResults(int page, String query);

    Observable<List<MovieDTO>> getMoreSearchResults(int page, String query);

    Observable<MoviesResponse> getFilmsByCategory(int page, int categoryId);

    Observable<List<QuerySuggestion>> findSuggestions(String query);

    Observable<Boolean> addToFavorites(String itemId, String type);

    Observable<List<MovieDTO>> getPlayListItems();

    Completable removeItemFromFavorite(String itemId);

    Completable removeAllItemsFromFavorite();

    MovieDTO getMovieInfo();

    MovieDTO getTVShowsInfo();

    MovieDTO getSearchInfo();

    void setLastMovieDetails(int categoryId, int lastMovieId);

    void setLastTVShowDetails(int categoryId, int lastTVShowId);

    void setLastSearchDetails(String query, int itemId);
}