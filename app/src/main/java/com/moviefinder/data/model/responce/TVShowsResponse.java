package com.moviefinder.data.model.responce;

import com.google.gson.annotations.SerializedName;
import com.moviefinder.data.model.content.TVShow;

import java.util.List;

public class TVShowsResponse {

    @SerializedName("results")
    private List<TVShow> tvShows;

    public List<TVShow> getTvShows() {
        return tvShows;
    }

    public void setTvShows(List<TVShow> tvShows) {
        this.tvShows = tvShows;
    }
}