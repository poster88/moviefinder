package com.moviefinder.data.model.responce;

import com.google.gson.annotations.SerializedName;
import com.moviefinder.data.model.content.MultiSearch;

import java.util.List;

public class SearchResponse {

    @SerializedName("page")
    private int page;
    @SerializedName("total_results")
    private int totalResults;
    @SerializedName("total_pages")
    private int totalPages;
    @SerializedName("results")
    private List<MultiSearch> results;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public List<MultiSearch> getResults() {
        return results;
    }

    public void setResults(List<MultiSearch> results) {
        this.results = results;
    }
}