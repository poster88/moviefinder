package com.moviefinder.ui.views;


public interface OnMenuItemClickListener {
    void onMenuItemClicked(int menuItemId, int itemId, int position);
}
