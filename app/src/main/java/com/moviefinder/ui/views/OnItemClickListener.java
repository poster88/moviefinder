package com.moviefinder.ui.views;

public interface OnItemClickListener {
    void onItemClicked(int itemId, String type);
}
