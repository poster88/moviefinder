package com.moviefinder.ui.views;

import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.DimenRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class ItemOffsetDecoration extends RecyclerView.ItemDecoration {

    public static final int ORIENTATION_HORIZONTAL = 0;
    public static final int ORIENTATION_VERTICAL = 1;

    private int mItemOffset;
    private int orientation = 0;

    public ItemOffsetDecoration(int itemOffset) {
        mItemOffset = itemOffset;
    }

    public ItemOffsetDecoration(@NonNull Context context, @DimenRes int itemOffsetId) {
        this(context.getResources().getDimensionPixelSize(itemOffsetId));
    }

    public ItemOffsetDecoration(@NonNull Context context, @DimenRes int itemOffsetId, int orientation) {
        this(context.getResources().getDimensionPixelSize(itemOffsetId));
        this.orientation = orientation;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        if (orientation == ORIENTATION_HORIZONTAL) {
            outRect.set(mItemOffset, 0, mItemOffset, 0);
        } else {
            outRect.set(mItemOffset, mItemOffset, mItemOffset, 0);
        }
    }
}