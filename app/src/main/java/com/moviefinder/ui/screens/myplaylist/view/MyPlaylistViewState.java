package com.moviefinder.ui.screens.myplaylist.view;

import com.hannesdorfmann.mosby3.mvp.viewstate.ViewState;

public interface MyPlaylistViewState extends ViewState<MyPlaylistView> {
    void showPlaylistState();

    void showNoResultState();
}
