package com.moviefinder.ui.screens.tvshowinfo.view;

import com.hannesdorfmann.mosby3.mvp.viewstate.ViewState;

import static com.moviefinder.utils.AppConstants.STATE_SHOW;

public class TVShowInfoViewStateImpl implements ViewState<TVShowInfoView>, TVShowInfoViewState {

    private int currentState;

    @Override
    public void apply(TVShowInfoView view, boolean retained) {
        if (currentState == STATE_SHOW) {
            view.restoreMovieState();
        }
    }

    @Override
    public void showMovieInfo() {
        currentState = STATE_SHOW;
    }
}