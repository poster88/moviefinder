package com.moviefinder.ui.screens.main;


import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.moviefinder.R;
import com.moviefinder.data.repository.MovieRepository;
import com.moviefinder.events.RetryConnectionsEvent;
import com.moviefinder.ui.screens.main.view.MainView;

import org.greenrobot.eventbus.EventBus;

public class MainPresenterImpl extends MvpBasePresenter<MainView> implements MainPresenter {

    public static final String TAG = "MainPresenterImpl";

    private final int PAGE_POSITION_MAIN = 0;
    private final int PAGE_POSITION_FILMS = 1;
    private final int PAGE_POSITION_TV_SHOWS = 2;

    private MovieRepository repository;
    private int defaultFabSheetViewId = R.id.fab_sheet_genre_all;
    private int pageSelected = PAGE_POSITION_MAIN;

    public MainPresenterImpl(MovieRepository repository) {
        this.repository = repository;
    }

    @Override
    public void onClickEvent(int resId) {
        if (resId == R.id.fragment_review_btn_more_films) {
            ifViewAttached(view -> view.setViewPagerCurrentItem(PAGE_POSITION_FILMS));
        } else if (resId == R.id.fragment_review_btn_more_tv_shows) {
            ifViewAttached(view -> view.setViewPagerCurrentItem(PAGE_POSITION_TV_SHOWS));
        } else if (resId == R.id.fragment_my_playlist_btn_go_main) {
            ifViewAttached(view -> view.setViewPagerCurrentItem(PAGE_POSITION_MAIN));
        } else {
            ifViewAttached(view -> view.setViewPagerCurrentItem(PAGE_POSITION_MAIN));
        }
    }

    @Override
    public void onViewPageSelected(int position) {
        pageSelected = position;
        setupFabVisibility(true);
        setupBottomNavigation();
    }

    @Override
    public void onFabSheetItemSelected(int viewId) {
        if (defaultFabSheetViewId != viewId) {
            ifViewAttached(view -> {
                view.changeDrawable(viewId, R.drawable.ic_crop_full_accent_dark_24dp);
                view.changeDrawable(defaultFabSheetViewId, R.drawable.ic_crop_empty_black_24dp);
            });
            defaultFabSheetViewId = viewId;
        }
    }

    @Override
    public void setupFabVisibility(boolean isVisible) {
        if (isVisible) {
            if (pageSelected == PAGE_POSITION_MAIN) {
                ifViewAttached(MainView::hideFab);
            } else {
                ifViewAttached(MainView::showFab);
            }
        } else {
            ifViewAttached(MainView::hideFab);
        }
    }

    @Override
    public void handleMessage(int resId) {
        if (resId == R.string.error_no_connection) {
            ifViewAttached(view -> view.showMessage(resId, R.string.title_retry));
        } else {
            ifViewAttached(view -> view.showMessage(resId));
        }
    }

    @Override
    public void onSnackBarClicked(int resId) {
        if (resId == R.string.error_no_connection) {
            ifViewAttached(view -> EventBus.getDefault().post(new RetryConnectionsEvent(true)));
        }
    }

    @Override
    public void setupBottomNavigation() {
        if (pageSelected == PAGE_POSITION_MAIN) {
            ifViewAttached(MainView::hideBottomNavigationView);
        } else {
            ifViewAttached(MainView::showBottomNavigationView);
        }
    }
}