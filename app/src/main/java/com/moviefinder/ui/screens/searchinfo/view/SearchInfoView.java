package com.moviefinder.ui.screens.searchinfo.view;

import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.moviefinder.data.mapper.MovieDTO;

public interface SearchInfoView extends MvpView {

    void restoreSearchInfoState();

    void removeFragment();

    void showSearchInfo(MovieDTO search);
}
