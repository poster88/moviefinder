package com.moviefinder.ui.screens.tvshow;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.moviefinder.ui.screens.tvshow.view.TVShowsView;

public interface TVShowsPresenter extends MvpPresenter<TVShowsView> {

    void onItemClicked(int itemId);

    void onMenuItemClicked(int menuItemId, int itemId, int position);

    void onScrolled(int visibleItemCount, int totalItemCount, int lastVisibleItemPosition, boolean isScrollDown);

    void onNewViewStateInstance();

    void onNavigationItemSelected(int event);

    void obFabSheetEvent(int viewId);

    void restoreTVShowsState();
}
