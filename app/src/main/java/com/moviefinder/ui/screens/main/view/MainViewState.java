package com.moviefinder.ui.screens.main.view;

import com.hannesdorfmann.mosby3.mvp.viewstate.ViewState;

public interface MainViewState extends ViewState<MainView> {

    void saveFabSheetItemSelectedState(int viewId);

    void isFabVisible(boolean isVisible);
}
