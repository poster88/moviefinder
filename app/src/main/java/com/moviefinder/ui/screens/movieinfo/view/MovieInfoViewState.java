package com.moviefinder.ui.screens.movieinfo.view;

import com.hannesdorfmann.mosby3.mvp.viewstate.ViewState;

public interface MovieInfoViewState extends ViewState<MovieInfoView> {

    void showMovieInfo();
}
