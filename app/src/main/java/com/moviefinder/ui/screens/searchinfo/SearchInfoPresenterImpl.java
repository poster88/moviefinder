package com.moviefinder.ui.screens.searchinfo;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.moviefinder.data.repository.MovieRepository;
import com.moviefinder.ui.screens.searchinfo.view.SearchInfoView;

public class SearchInfoPresenterImpl extends MvpBasePresenter<SearchInfoView>
        implements SearchInfoPresenter {

    public static final String TAG = "SearchInfoPresenterImpl";

    private MovieRepository repository;

    public SearchInfoPresenterImpl(MovieRepository repository) {
        this.repository = repository;
    }

    @Override
    public void onNavigationBtnClick() {
        ifViewAttached(SearchInfoView::removeFragment);
    }

    @Override
    public void onNewViewStateInstance() {
        showSearchInfo();
    }

    private void showSearchInfo() {
        ifViewAttached(view -> view.showSearchInfo(repository.getSearchInfo()));
    }

    @Override
    public void restoreSearchInfoState() {
        showSearchInfo();
    }
}
