package com.moviefinder.ui.screens.review.view;


import com.moviefinder.R;

import static com.moviefinder.utils.AppConstants.STATE_DEFAULT;
import static com.moviefinder.utils.AppConstants.STATE_ERROR;
import static com.moviefinder.utils.AppConstants.STATE_IS_LOADING;
import static com.moviefinder.utils.AppConstants.STATE_SHOW;

public class ReviewViewStateImpl implements ReviewViewState {

    public static final String TAG = "ReviewViewStateImpl";

    private int moviesState = STATE_DEFAULT;
    private int tvShowsState = STATE_DEFAULT;
    private int errorResId = R.string.error_default;

    @Override
    public void apply(ReviewView view, boolean retained) {
        setMoviesState(view);
        setTVShowsState(view);
    }

    private void setTVShowsState(ReviewView view) {
        if (tvShowsState == STATE_IS_LOADING) {
            view.showTVShowsLoading();
        } else if (tvShowsState == STATE_SHOW) {
            view.restoreTVShowsState();
        } else if (tvShowsState == STATE_ERROR) {
            handleMovieError(view);
        }
    }

    private void setMoviesState(ReviewView view) {
        if (moviesState == STATE_IS_LOADING) {
            view.showMoviesLoading();
        } else if (moviesState == STATE_SHOW) {
            view.restoreMoviesState();
        } else if (moviesState == STATE_ERROR) {
            handleMovieError(view);
        }
    }

    private void handleMovieError(ReviewView view) {
        if (errorResId == R.string.error_no_connection) {
            view.showError(errorResId);
        }
    }

    @Override
    public void setStateShowList() {
        moviesState = STATE_SHOW;
    }

    @Override
    public void setStateShowLoading() {
        moviesState = STATE_IS_LOADING;
    }

    @Override
    public void setStateError(int errorResId) {
        this.errorResId = errorResId;
        moviesState = STATE_ERROR;
    }

    @Override
    public void setStateTVShows() {
        tvShowsState = STATE_SHOW;
    }

    @Override
    public void setStateTVShowLoading() {
        tvShowsState = STATE_IS_LOADING;
    }

    @Override
    public void setStateDefault() {
        moviesState = STATE_DEFAULT;
        tvShowsState = STATE_DEFAULT;
    }
}