package com.moviefinder.ui.screens.search;

import android.util.Log;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.moviefinder.R;
import com.moviefinder.data.repository.MovieRepository;
import com.moviefinder.events.ButtonClickEvent;
import com.moviefinder.events.MessageEvent;
import com.moviefinder.ui.screens.search.view.SearchView;
import com.moviefinder.utils.AppConstants;
import com.moviefinder.utils.ServerException;

import org.greenrobot.eventbus.EventBus;

import java.util.List;
import java.util.concurrent.TimeUnit;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

import static com.moviefinder.utils.AppConstants.DEFAULT_PAGE;
import static com.moviefinder.utils.AppConstants.FADE_DIM_BACKGROUND_END;
import static com.moviefinder.utils.AppConstants.FADE_DIM_BACKGROUND_START;

public class SearchPresenterImpl extends MvpBasePresenter<SearchView> implements SearchPresenter {

    public static final String TAG = "SearchPresenterImpl";

    private MovieRepository repository;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private PublishSubject<String> querySubject = PublishSubject.create();
    private int searchPage = DEFAULT_PAGE;
    private String oldQuery;
    private boolean isContentLoading;
    private int categoryId = R.id.action_actual;


    public SearchPresenterImpl(MovieRepository repository) {
        this.repository = repository;
    }

    @Override
    public void onNewViewStateInstance() {
        compositeDisposable.add(startQueryListener());
    }

    private Disposable startQueryListener() {
        return querySubject.debounce(AppConstants.SEARCH_DELAY, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .filter(query -> !query.isEmpty())
                .flatMap((Function<String, ObservableSource<List<QuerySuggestion>>>) query ->
                        repository.findSuggestions(query))
                .subscribe(querySuggestions -> ifViewAttached(view ->
                        view.swapSearchSuggestion(querySuggestions)));
    }

    @Override
    public void onSearchAction(String query) {
        if (!query.isEmpty()) {
            oldQuery = query;
            searchPage = DEFAULT_PAGE;
            compositeDisposable.add(getSearchResults(query));
        }
    }

    private Disposable getSearchResults(String query) {
        return repository.getSearchResults(searchPage, query)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> ifViewAttached(view -> {
                    view.fadeDimBackground(FADE_DIM_BACKGROUND_END, FADE_DIM_BACKGROUND_START);
                    view.showComponentContainer();
                }))
                .doOnNext(multiSearches -> {
                    if (multiSearches.isEmpty()) ifViewAttached(SearchView::showNoResults);
                })
                .subscribe(searchResult -> ifViewAttached(view -> view.showSearchResults(searchResult)),
                        throwable -> {
                            handleException(throwable);
                            Log.d(TAG, "getSearchResults error: " + throwable.fillInStackTrace());
                        });
    }

    @Override
    public void onScrolled(int lastVisibleItemPosition, int totalItemCount, boolean isScrollDown) {
        boolean isLoadMore = !isContentLoading && lastVisibleItemPosition == totalItemCount - 1
                && isScrollDown;
        if (isLoadMore) compositeDisposable.add(loadLoreContent(searchPage));
    }

    private Disposable loadLoreContent(int page) {
        return repository.getMoreSearchResults(++page, oldQuery)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> {
                    isContentLoading = true;
                    ifViewAttached(SearchView::showLoadingMorePages);
                })
                .doFinally(() -> isContentLoading = false)
                .subscribe(multiSearches -> {
                    ifViewAttached(view -> {
                        view.hideLoadingMorePages();
                        view.setData(multiSearches);
                    });
                    ++searchPage;
                }, throwable -> {
                    handleException(throwable);
                    ifViewAttached(SearchView::hideLoadingMorePages);
                });
    }

    private void handleException(Throwable throwable) {
        ServerException error = (ServerException) throwable;
        int errorId = R.string.error_default;
        if (error.getKind() == ServerException.Kind.NETWORK) {
            errorId = R.string.error_no_connection;
        } else if (error.getKind() == ServerException.Kind.HTTP) {
            errorId = R.string.error_bad_http_request;
        } else if (error.getKind() == ServerException.Kind.UNEXPECTED) {
            errorId = R.string.error_unexpected;
        }
        EventBus.getDefault().post(new MessageEvent(errorId));
    }

    @Override
    public void onSearchFocus(boolean isComponentsLayoutVisible) {
        if (!isComponentsLayoutVisible) {
            ifViewAttached(view -> view.fadeDimBackground(FADE_DIM_BACKGROUND_START,
                    FADE_DIM_BACKGROUND_END));
        }
    }

    @Override
    public void onSearchFocusCleared(boolean isComponentsLayoutVisible) {
        if (!isComponentsLayoutVisible) {
            ifViewAttached(view -> {
                view.clearSuggestions();
                view.clearSearchView();
                view.fadeDimBackground(FADE_DIM_BACKGROUND_END, FADE_DIM_BACKGROUND_START);
            });
        }
    }

    @Override
    public void onQueryChange(String query) {
        querySubject.onNext(query);
    }

    @Override
    public void onSuggestionClicked(QuerySuggestion suggestion) {
        onSearchAction(suggestion.getBody());
    }

    @Override
    public void restoreSearchResults() {
        compositeDisposable.add(getSearchResults(oldQuery));
    }

    @Override
    public void onItemClicked(int itemId, String type) {
        repository.setLastSearchDetails(oldQuery, itemId);
        ifViewAttached(SearchView::showSearchInfo);
    }

    @Override
    public void onMenuItemClicked(int menuItemId, int itemId, String type) {
        if (menuItemId == R.id.action_add_to_favorite) {
            repository.setLastSearchDetails(oldQuery, itemId);
            compositeDisposable.add(addSearchItemToFavorite(String.valueOf(itemId), AppConstants.TYPE_SEARCH_RESULT));
        }
    }

    private Disposable addSearchItemToFavorite(String itemId, String key) {
        return repository.addToFavorites(itemId, key)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(isItemAdded -> {
                    if (isItemAdded) {
                        EventBus.getDefault().post(new MessageEvent(R.string.add_to_favorite));
                    } else {
                        EventBus.getDefault().post(new MessageEvent(R.string.fail_add_to_favorite));
                    }
                });
    }

    @Override
    public void onActionItemSelected(int itemId) {
        if (itemId == R.id.action_my_play_lists) {
            ifViewAttached(SearchView::showMyPlayListScreen);
            ifViewAttached(SearchView::onBackPress);
            EventBus.getDefault().post(new ButtonClickEvent(itemId));
        }
    }
}