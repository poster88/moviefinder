package com.moviefinder.ui.screens.myplaylist;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RatingBar;
import android.widget.TextView;

import com.moviefinder.R;
import com.moviefinder.data.mapper.MovieDTO;
import com.moviefinder.ui.views.OnItemClickListener;
import com.moviefinder.ui.views.OnMenuItemClickListener;
import com.moviefinder.utils.ViewUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyPlayListAdapter extends RecyclerView.Adapter<MyPlayListAdapter.MyPlayListHolder> {

    public static final String TAG = "MyPlayListAdapter";

    private List<MovieDTO> data = new ArrayList<>();
    private OnItemClickListener onItemClickListener;
    private OnMenuItemClickListener onMenuItemClickListener;
    private PopupMenu popupMenu;

    @NonNull
    @Override
    public MyPlayListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_play_list, parent, false);
        return new MyPlayListHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyPlayListHolder h, int position) {
        MovieDTO movie = data.get(position);
        ViewUtils.loadImage(h.imgPoster, movie.getPosterPath(), R.drawable.ic_square_placeholder_light_gray);
        h.textTitle.setText(movie.getTitle());
        h.textGenres.setText(movie.getStringGenres());
        h.ratingBar.setRating(movie.getFormattedVoteAverage());
        h.textOverView.setText(movie.getOverview());
        h.textDate.setText(movie.getFormattedDate());
        h.itemView.setOnClickListener(v -> onItemClickListener.onItemClicked(movie.getId(), movie.getMediaType()));
        h.imgMenu.setOnClickListener(v -> {
            popupMenu = new PopupMenu(v.getContext(), v);
            popupMenu.inflate(R.menu.menu_context_playlist);
            popupMenu.show();
            popupMenu.setOnMenuItemClickListener(item -> {
                onMenuItemClickListener.onMenuItemClicked(item.getItemId(), movie.getId(), position);
                return true;
            });
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setOnMenuItemClickListener(OnMenuItemClickListener onMenuItemClickListener) {
        this.onMenuItemClickListener = onMenuItemClickListener;
    }

    public void setData(List<MovieDTO> list) {
        data.clear();
        data.addAll(list);
        notifyDataSetChanged();
    }

    public void removeAll() {
        data.clear();
        notifyDataSetChanged();
    }

    public void removeItem(int position) {
        data.remove(position);
        notifyDataSetChanged();
    }

    class MyPlayListHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_play_list_img_poster) ImageView imgPoster;
        @BindView(R.id.item_play_list_title) TextView textTitle;
        @BindView(R.id.item_play_list_img_btn_menu) ImageView imgMenu;
        @BindView(R.id.item_play_list_text_genre) TextView textGenres;
        @BindView(R.id.item_play_list_rb_rating) RatingBar ratingBar;
        @BindView(R.id.item_play_list_overview) TextView textOverView;
        @BindView(R.id.item_play_list_text_release_date) TextView textDate;

        MyPlayListHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}