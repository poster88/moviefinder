package com.moviefinder.ui.screens.main;


import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.moviefinder.ui.screens.main.view.MainView;

public interface MainPresenter extends MvpPresenter<MainView> {

    void onClickEvent(int resId);

    void onViewPageSelected(int position);

    void onFabSheetItemSelected(int viewId);

    void setupFabVisibility(boolean isVisible);

    void handleMessage(int resId);

    void onSnackBarClicked(int resId);

    void setupBottomNavigation();
}