package com.moviefinder.ui.screens.search;

import android.os.Parcel;

import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;


public class QuerySuggestion implements SearchSuggestion {

    private String name;
    private boolean isHistory;


    public QuerySuggestion(String name, boolean isHistory) {
        this.name = name;
        this.isHistory = isHistory;
    }

    public QuerySuggestion(Parcel parcel) {
        this.name = parcel.readString();
        this.isHistory = parcel.readInt() != 0;
    }

    @Override
    public String getBody() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getIsHistory() {
        return isHistory;
    }

    public void setHistory(boolean history) {
        isHistory = history;
    }

    public static final Creator<QuerySuggestion> CREATOR = new Creator<QuerySuggestion>() {
        @Override
        public QuerySuggestion createFromParcel(Parcel source) {
            return new QuerySuggestion(source);
        }

        @Override
        public QuerySuggestion[] newArray(int size) {
            return new QuerySuggestion[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(isHistory ? 1 : 0);
    }
}