package com.moviefinder.ui.screens.review.view;


import com.hannesdorfmann.mosby3.mvp.viewstate.ViewState;

public interface ReviewViewState extends ViewState<ReviewView> {

    void setStateShowList();

    void setStateShowLoading();

    void setStateError(int errorResId);

    void setStateTVShows();

    void setStateTVShowLoading();

    void setStateDefault();
}
