package com.moviefinder.ui.screens.search.view;


import com.moviefinder.utils.AppConstants;

public class SearchViewStateImpl implements SearchViewState {

    private final int STATE_FADE_DIM_ENABLED = 1;
    private final int STATE_FADE_DIM_DISABLED = -1;
    private final int STATE_SHOW_SEARCH_RESULTS = 1;
    private final int STATE_SHOW_SEARCH_LOADING = 2;
    private final int STATE_SHOW_SEARCH_DISABLED = -1;
    private final int STATE_SHOW_SEARCH_NO_RESULTS = 3;
    private int stateSearchResults;
    private int stateFadeDim;

    @Override
    public void apply(SearchView view, boolean retained) {
        restoreDimState(view, stateFadeDim);
        restoreSearchResultsState(view, stateSearchResults);
    }

    private void restoreSearchResultsState(SearchView view, int stateSearchResults) {
        if (stateSearchResults == STATE_SHOW_SEARCH_LOADING) {
            view.showComponentContainer();
        } else if (stateSearchResults == STATE_SHOW_SEARCH_RESULTS) {
            view.restoreSearchResults();
        } else if (stateSearchResults == STATE_SHOW_SEARCH_NO_RESULTS) {
            view.showNoResults();
        }
    }

    private void restoreDimState(SearchView view, int stateFadeDim) {
        if (stateFadeDim == STATE_FADE_DIM_ENABLED) {
            view.fadeDimBackground(AppConstants.FADE_DIM_BACKGROUND_START,
                    AppConstants.FADE_DIM_BACKGROUND_END);
        }
    }

    @Override
    public void setStateFadeDimBackground(int value) {
        stateFadeDim = value == AppConstants.FADE_DIM_BACKGROUND_START
                ? STATE_FADE_DIM_ENABLED : STATE_FADE_DIM_DISABLED;
    }

    @Override
    public void setStateShowSearchResults() {
        stateSearchResults = STATE_SHOW_SEARCH_RESULTS;
    }

    @Override
    public void setStateShowResultContainer() {
        stateSearchResults = STATE_SHOW_SEARCH_LOADING;
    }

    @Override
    public void setStateHideContainer() {
        stateSearchResults = STATE_SHOW_SEARCH_DISABLED;
    }

    @Override
    public void setStateNoResults() {
        stateSearchResults = STATE_SHOW_SEARCH_NO_RESULTS;
    }
}