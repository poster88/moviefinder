package com.moviefinder.ui.screens.tvshowinfo.view;

import com.hannesdorfmann.mosby3.mvp.viewstate.ViewState;

public interface TVShowInfoViewState extends ViewState<TVShowInfoView> {

    void showMovieInfo();
}
