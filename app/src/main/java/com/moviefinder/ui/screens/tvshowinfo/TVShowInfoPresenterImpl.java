package com.moviefinder.ui.screens.tvshowinfo;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.moviefinder.data.repository.MovieRepository;
import com.moviefinder.ui.screens.tvshowinfo.view.TVShowInfoView;


public class TVShowInfoPresenterImpl extends MvpBasePresenter<TVShowInfoView> implements TVShowInfoPresenter {

    public static final String TAG = "TVShowInfoPresenterImpl";

    private MovieRepository repository;

    public TVShowInfoPresenterImpl(MovieRepository repository) {
        this.repository = repository;
    }

    @Override
    public void onNavigationBtnClick() {
        ifViewAttached(TVShowInfoView::removeFragment);
    }

    @Override
    public void onNewViewStateInstance() {
        showMovieInfo();
    }

    @Override
    public void restoreMovieInfoState() {
        showMovieInfo();
    }

    private void showMovieInfo() {
        ifViewAttached(view -> view.showMovieInfo(repository.getTVShowsInfo()));
    }
}