package com.moviefinder.ui.screens.myplaylist.view;

public class MyPlaylistViewStateImpl implements MyPlaylistViewState {

    private int STATE_SHOW_LIST = 1;
    private int STATE_SHOW_NO_RESULTS = 2;
    private int STATE_ERROR = -1;
    private int currentState;

    @Override
    public void apply(MyPlaylistView view, boolean retained) {
        if (currentState == STATE_SHOW_LIST) {
            view.restoreListState();
        } else if (currentState == STATE_SHOW_NO_RESULTS) {
            view.showNoResultView();
        }
    }

    @Override
    public void showPlaylistState() {
        currentState = STATE_SHOW_LIST;
    }

    @Override
    public void showNoResultState() {
        currentState = STATE_SHOW_NO_RESULTS;
    }
}
