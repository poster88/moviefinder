package com.moviefinder.ui.screens.films.view;

import com.hannesdorfmann.mosby3.mvp.viewstate.ViewState;

public interface FilmsViewState extends ViewState<FilmsView> {

    void setShowLoadingState();

    void setShowLoadingMorePagesState();

    void setShowFilmsState();

    void setStateNoInternetConnections();

    void setStateDefault();
}
