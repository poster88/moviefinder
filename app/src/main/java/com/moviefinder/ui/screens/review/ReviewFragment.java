package com.moviefinder.ui.screens.review;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.moviefinder.MovieApp;
import com.moviefinder.data.mapper.MovieDTO;
import com.moviefinder.events.ButtonClickEvent;
import com.moviefinder.R;
import com.moviefinder.events.MessageEvent;
import com.moviefinder.events.RetryConnectionsEvent;
import com.moviefinder.ui.BaseFragment;
import com.moviefinder.ui.screens.movieinfo.MovieInfoFragment;
import com.moviefinder.ui.screens.review.view.ReviewView;
import com.moviefinder.ui.screens.review.view.ReviewViewState;
import com.moviefinder.ui.screens.review.view.ReviewViewStateImpl;
import com.moviefinder.ui.screens.tvshowinfo.TVShowInfoFragment;
import com.moviefinder.ui.views.ItemOffsetDecoration;
import com.moviefinder.utils.ViewUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class ReviewFragment extends BaseFragment<ReviewView, ReviewPresenter, ReviewViewState>
        implements ReviewView {

    public static final String TAG = "ReviewFragment";

    public static ReviewFragment newInstance() {
        Bundle args = new Bundle();
        ReviewFragment fragment = new ReviewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.fragment_review_rv_movies) RecyclerView rvMovies;
    @BindView(R.id.fragment_review_text_title_films) TextView textTitleFilms;
    @BindView(R.id.item_card_pb_films_loading) ProgressBar pbFilmsLoading;
    @BindView(R.id.fragment_review_rv_tv_shows) RecyclerView rvTVShows;
    @BindView(R.id.fragment_review_text_title_tv_shows) TextView textTitleTVShows;
    @BindView(R.id.item_card_pb_films_tv_shows) ProgressBar pbTVShowsLoading;

    private Unbinder unbinder;
    private MoviesAdapter moviesAdapter;
    private MoviesAdapter tvShowsAdapter;
    private LinearLayoutManager moviesLayoutManager;
    private LinearLayoutManager tvShowsLayoutManager;
    private ItemOffsetDecoration itemDecoration;

    @NonNull
    @Override
    public ReviewPresenter createPresenter() {
        return new ReviewPresenterImpl(MovieApp.provideMovieRepository());
    }

    @NonNull
    @Override
    public ReviewViewStateImpl createViewState() {
        return new ReviewViewStateImpl();
    }

    @Override
    public void onNewViewStateInstance() {
        presenter.onNewViewStateInstance();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_review, container, false);
        unbinder = ButterKnife.bind(this, view);
        initComponents();
        setupAdapters();
        setupRecycleViews();
        return view;
    }

    private void setupAdapters() {
        tvShowsAdapter = new MoviesAdapter();
        tvShowsAdapter.setItemClickListener((itemId, type) ->
                presenter.onTVShowItemClicked(itemId, type));
        tvShowsAdapter.setItemMenuClickListener((menuItemId, itemId, position) ->
                presenter.onTVShowMenuItemClicked(menuItemId, itemId, position));
        moviesAdapter = new MoviesAdapter();
        moviesAdapter.setItemClickListener((itemId, type) ->
                presenter.onMovieItemClicked(itemId, type));
        moviesAdapter.setItemMenuClickListener((menuItemId, itemId, position) ->
                presenter.onMovieMenuItemClicked(menuItemId, itemId, position));
    }

    private void initComponents() {
        moviesLayoutManager = new LinearLayoutManager(getContext(),
                LinearLayoutManager.HORIZONTAL, false);
        tvShowsLayoutManager = new LinearLayoutManager(getContext(),
                LinearLayoutManager.HORIZONTAL, false);
        itemDecoration = new ItemOffsetDecoration(Objects.requireNonNull(getContext()),
                R.dimen.item_rv_offset);
    }

    private void setupRecycleViews() {
        rvTVShows.addItemDecoration(itemDecoration);
        rvTVShows.setLayoutManager(tvShowsLayoutManager);
        rvTVShows.setAdapter(tvShowsAdapter);
        rvMovies.addItemDecoration(itemDecoration);
        rvMovies.setLayoutManager(moviesLayoutManager);
        rvMovies.setAdapter(moviesAdapter);
    }

    @Override
    public void showMovies(List<MovieDTO> movies) {
        viewState.setStateShowList();
        moviesAdapter.setData(movies);
        rvMovies.setVisibility(View.VISIBLE);
    }

    @Override
    public void showMoviesLoading() {
        viewState.setStateShowLoading();
        pbFilmsLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideMoviesLoading() {
        pbFilmsLoading.setVisibility(View.GONE);
    }

    @Override
    public void restoreMoviesState() {
        presenter.restoreMovies();
    }

    @Override
    public void showError(int errorResId) {
        viewState.setStateError(errorResId);
        MessageEvent event = new MessageEvent(errorResId);
        EventBus.getDefault().post(event);
    }

    @Override
    public void showMovieInfoScreen() {
        ViewUtils.replaceFragment(Objects.requireNonNull(getFragmentManager()),
                MovieInfoFragment.newInstance(), getString(R.string.tag_movie_info));
    }

    @Override
    public void showTVShows(List<MovieDTO> tvShows) {
        viewState.setStateTVShows();
        tvShowsAdapter.setData(tvShows);
        rvTVShows.setVisibility(View.VISIBLE);
    }

    @Override
    public void showTVShowsLoading() {
        viewState.setStateTVShowLoading();
        pbTVShowsLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideTVShowLoading() {
        pbTVShowsLoading.setVisibility(View.GONE);
    }

    @Override
    public void restoreTVShowsState() {
        presenter.restoreTVShows();
    }

    @Override
    public void showTVShowsScreen() {
        ViewUtils.replaceFragment(Objects.requireNonNull(getFragmentManager()),
                TVShowInfoFragment.newInstance(), getString(R.string.tag_tv_show_info_fragment));
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        presenter.attachView(this);
    }

    @OnClick({R.id.fragment_review_btn_more_films, R.id.fragment_review_btn_more_tv_shows})
    public void onBtnMoreFilmsClick(View view) {
        ButtonClickEvent event = new ButtonClickEvent(view.getId());
        EventBus.getDefault().post(event);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleConnectionEvent(RetryConnectionsEvent event) {
        presenter.handleConnectionEvent(event.isRetryConnection());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}