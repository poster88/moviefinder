package com.moviefinder.ui.screens.tvshow.view;

import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.moviefinder.data.mapper.MovieDTO;

import java.util.List;

public interface TVShowsView extends MvpView {

    void showTVShowInfoScreen();

    void showLoading();

    void hideLoading();

    void showNoInternetConnectionView();

    void showTVShows(List<MovieDTO> tvShows);

    void showLoadingMorePages();

    void hideLoadingMorePages();

    void setData(List<MovieDTO> shows);

    void refreshScrollPosition();

    void hideNoInternetConnectionView();

    void restoreTVShowsState();
}
