package com.moviefinder.ui.screens.tvshow.view;

import com.hannesdorfmann.mosby3.mvp.viewstate.ViewState;

public interface TVShowsState extends ViewState<TVShowsView> {

    void setStateDefault();

    void setShowLoadingState();

    void setShowLoadingMorePagesState();

    void setShowTVShowsState();

    void setStateNoInternetConnections();
}
