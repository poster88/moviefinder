package com.moviefinder.ui.screens.searchinfo.view;

import com.hannesdorfmann.mosby3.mvp.viewstate.ViewState;

public interface SearchInfoViewState extends ViewState<SearchInfoView> {
    void setShowSearchInfo();
}
