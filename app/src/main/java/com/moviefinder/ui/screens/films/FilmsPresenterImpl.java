package com.moviefinder.ui.screens.films;


import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.moviefinder.R;
import com.moviefinder.data.repository.MovieRepository;
import com.moviefinder.events.MessageEvent;
import com.moviefinder.ui.screens.films.view.FilmsView;
import com.moviefinder.utils.AppConstants;
import com.moviefinder.utils.ServerException;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.moviefinder.utils.AppConstants.DEFAULT_PAGE;

public class FilmsPresenterImpl extends MvpBasePresenter<FilmsView> implements FilmsPresenter {

    public static final String TAG = "FilmsPresenterImpl";

    private MovieRepository repository;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private int pageActual = DEFAULT_PAGE;
    private int pageRated = DEFAULT_PAGE;
    private int pageInCinema = DEFAULT_PAGE;
    private int pageNowPlaying = DEFAULT_PAGE;
    private int filterGenreId = R.id.fab_sheet_genre_all;
    private int categoryId = R.id.action_actual;
    private boolean isContentLoading;

    public FilmsPresenterImpl(MovieRepository repository) {
        this.repository = repository;
    }

    @Override
    public void onNewViewStateInstance() {
        compositeDisposable.add(showFilms());
    }

    private Disposable showFilms() {
        return repository.getFilms(getPage(categoryId, false), categoryId, filterGenreId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> ifViewAttached(FilmsView::showLoading))
                .doAfterTerminate(() -> ifViewAttached(FilmsView::hideLoading))
                .onErrorReturn(throwable -> {
                    ServerException error = (ServerException) throwable;
                    if (error.getKind() == ServerException.Kind.NETWORK) {
                        ifViewAttached(FilmsView::showNoInternetConnectionView);
                    }
                    return new ArrayList<>();
                })
                .subscribe(movies -> ifViewAttached(view -> view.showFilms(movies)));
    }

    @Override
    public void restoreFilmsState() {
        compositeDisposable.add(showFilms());
    }

    @Override
    public void onScrolled(int visibleItemCount, int totalItemCount, int lastVisibleItemPosition, boolean isScrollDown) {
        boolean isLoadMore = !isContentLoading && (visibleItemCount + lastVisibleItemPosition) >= totalItemCount
                && isScrollDown;
        if (isLoadMore) compositeDisposable.add(loadMoreContent(getPage(categoryId, false)));
    }

    private Disposable loadMoreContent(int page) {
        return repository.getMoreFilms(++page, categoryId, filterGenreId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> {
                    isContentLoading = true;
                    ifViewAttached(FilmsView::showLoadingMorePages);
                })
                .doFinally(() -> {
                    isContentLoading = false;
                    ifViewAttached(FilmsView::hideLoadingMorePages);
                })
                .subscribe(movies -> {
                    getPage(categoryId, true);
                    ifViewAttached(view -> view.setData(movies));
                }, this::handleException);
    }

    private void handleException(Throwable throwable) {
        ServerException error = (ServerException) throwable;
        int errorId = R.string.error_default;
        if (error.getKind() == ServerException.Kind.NETWORK) {
            errorId = R.string.error_no_connection;
        } else if (error.getKind() == ServerException.Kind.HTTP) {
            errorId = R.string.error_bad_http_request;
        } else if (error.getKind() == ServerException.Kind.UNEXPECTED) {
            errorId = R.string.error_unexpected;
        }
        EventBus.getDefault().post(new MessageEvent(errorId));
    }

    @Override
    public void onNavigationItemSelected(int itemId) {
        categoryId = itemId;
        compositeDisposable.add(showFilms());
        ifViewAttached(FilmsView::refreshScrollPosition);
        ifViewAttached(FilmsView::hideNoInternetConnectionView);
    }

    @Override
    public void obFabSheetEvent(int viewId) {
        if (filterGenreId != viewId) {
            filterGenreId = viewId;
            compositeDisposable.add(showFilms());
        }
    }

    @Override
    public void onItemClicked(int itemId) {
        repository.setLastMovieDetails(categoryId, itemId);
        ifViewAttached(FilmsView::showMovieInfoScreen);
    }

    @Override
    public void onMenuItemClicked(int menuItemId, int itemId, int position) {
        if (menuItemId == R.id.action_add_to_favorite) {
            repository.setLastMovieDetails(categoryId, itemId);
            compositeDisposable.add(addToItemToFavorite(String.valueOf(itemId), AppConstants.TYPE_MOVIE));
        }
    }

    private Disposable addToItemToFavorite(String itemId, String type) {
        return repository.addToFavorites(itemId, type)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(isItemAdded -> {
                    if (isItemAdded) {
                        EventBus.getDefault().post(new MessageEvent(R.string.add_to_favorite));
                    } else {
                        EventBus.getDefault().post(new MessageEvent(R.string.fail_add_to_favorite));
                    }
                });
    }

    private int getPage(int categoryId, boolean isIncrease) {
        if (categoryId == R.id.action_actual) {
            return isIncrease ? ++pageActual : pageActual;
        } else if (categoryId == R.id.action_rated) {
            return isIncrease ? ++pageRated : pageRated;
        } else if (categoryId == R.id.action_in_release) {
            return isIncrease ? ++pageInCinema : pageInCinema;
        } else {
            return isIncrease ? ++pageNowPlaying : pageNowPlaying;
        }
    }
}