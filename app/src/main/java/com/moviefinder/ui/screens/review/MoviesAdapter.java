package com.moviefinder.ui.screens.review;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RatingBar;
import android.widget.TextView;

import com.moviefinder.R;
import com.moviefinder.data.mapper.MovieDTO;
import com.moviefinder.ui.views.OnItemClickListener;
import com.moviefinder.ui.views.OnMenuItemClickListener;
import com.moviefinder.utils.ViewUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.ReviewMovieHolder> {

    private List<MovieDTO> data = new ArrayList<>();
    private OnItemClickListener itemClickListener;
    private OnMenuItemClickListener itemMenuClickListener;
    private PopupMenu popupMenu;

    @NonNull
    @Override
    public ReviewMovieHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_moview_review, parent, false);
        return new ReviewMovieHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReviewMovieHolder h, final int position) {
        MovieDTO movieDTO = data.get(position);
        ViewUtils.loadImage(h.imgPoster, movieDTO.getPosterPath(), R.drawable.ic_square_placeholder_light_gray);
        h.textTitle.setText(movieDTO.getTitle());
        h.textGenre.setText(movieDTO.getStringGenres());
        h.rbMovieRating.setRating(movieDTO.getFormattedVoteAverage());
        h.itemView.setOnClickListener(v ->
                itemClickListener.onItemClicked(movieDTO.getId(), movieDTO.getMediaType()));
        h.imgMenu.setOnClickListener(v -> {
            popupMenu = new PopupMenu(v.getContext(), v);
            popupMenu.inflate(R.menu.menu_context_movie);
            popupMenu.show();
            popupMenu.setOnMenuItemClickListener(item -> {
                itemMenuClickListener.onMenuItemClicked(item.getItemId(), movieDTO.getId(), position);
                return true;
            });
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<MovieDTO> list) {
        data.clear();
        data.addAll(list);
        notifyDataSetChanged();
    }

    public void setItemClickListener(@NonNull OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public void setItemMenuClickListener(@NonNull OnMenuItemClickListener itemMenuClickListener) {
        this.itemMenuClickListener = itemMenuClickListener;
    }

    class ReviewMovieHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_img_poster) ImageView imgPoster;
        @BindView(R.id.item_img_btn_menu) ImageView imgMenu;
        @BindView(R.id.item_text_title) TextView textTitle;
        @BindView(R.id.item_text_genre) TextView textGenre;
        @BindView(R.id.item_rb_rating) RatingBar rbMovieRating;

        ReviewMovieHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}