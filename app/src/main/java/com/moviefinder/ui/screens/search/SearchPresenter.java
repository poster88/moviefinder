package com.moviefinder.ui.screens.search;


import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.moviefinder.ui.screens.search.view.SearchView;

public interface SearchPresenter extends MvpPresenter<SearchView> {

    void onSearchFocus(boolean isComponentsLayoutVisible);

    void onSearchFocusCleared(boolean isComponentsLayoutVisible);

    void onNewViewStateInstance();

    void onSearchAction(String query);

    void onActionItemSelected(int itemId);

    void onScrolled(int lastVisibleItemPosition, int itemCount, boolean isScrollDown);

    void restoreSearchResults();

    void onItemClicked(int itemId, String type);

    void onMenuItemClicked(int menuItemId, int itemId, String type);

    void onQueryChange(String query);

    void onSuggestionClicked(QuerySuggestion suggestion);
}
