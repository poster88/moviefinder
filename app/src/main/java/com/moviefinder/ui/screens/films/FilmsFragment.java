package com.moviefinder.ui.screens.films;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.moviefinder.MovieApp;
import com.moviefinder.R;
import com.moviefinder.data.mapper.MovieDTO;
import com.moviefinder.events.BottomNavigationItemSelected;
import com.moviefinder.events.FabEvent;
import com.moviefinder.events.FabSheetEvent;
import com.moviefinder.ui.BaseFragment;
import com.moviefinder.ui.screens.films.view.FilmsStateImpl;
import com.moviefinder.ui.screens.films.view.FilmsView;
import com.moviefinder.ui.screens.films.view.FilmsViewState;
import com.moviefinder.ui.screens.movieinfo.MovieInfoFragment;
import com.moviefinder.ui.views.EqualSpacingItemDecoration;
import com.moviefinder.utils.AppConstants;
import com.moviefinder.utils.ViewUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class FilmsFragment extends BaseFragment<FilmsView, FilmsPresenter, FilmsViewState>
        implements FilmsView {

    public static final String TAG = "FilmsFragment";

    public static FilmsFragment newInstance() {
        Bundle args = new Bundle();
        FilmsFragment fragment = new FilmsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.fragment_films_rv_films_list) RecyclerView rvFilms;
    @BindView(R.id.fragment_films_pb_loading) ProgressBar pbLoading;
    @BindView(R.id.fragment_films_pb_loading_content) ProgressBar pbLoadingContent;
    @BindView(R.id.fragment_films_no_internet_connection) LinearLayout layoutNoInternetConnections;

    private Unbinder unbinder;
    private FilmsAdapter adapter;
    private GridLayoutManager layoutManager;
    private EqualSpacingItemDecoration itemDecoration;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_films, container, false);
        unbinder = ButterKnife.bind(this, view);
        initComponents();
        setupRecycleView();
        return view;
    }

    private void initComponents() {
        layoutManager = new GridLayoutManager(getContext(), getResources().getInteger(R.integer.columns_count));
        itemDecoration = new EqualSpacingItemDecoration(getResources().getInteger(R.integer.columns_spacing), EqualSpacingItemDecoration.GRID);
        adapter = createAdapter();
        adapter.setItemClickListener((itemId, type) -> presenter.onItemClicked(itemId));
        adapter.setMenuItemClickListener((menuItemId, itemId, position) ->
                presenter.onMenuItemClicked(menuItemId, itemId, position));
    }

    private void setupRecycleView() {
        rvFilms.addItemDecoration(itemDecoration);
        rvFilms.setLayoutManager(layoutManager);
        rvFilms.setAdapter(adapter);
        rvFilms.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int lastVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
                presenter.onScrolled(visibleItemCount, totalItemCount, lastVisibleItemPosition, dy > 0);
            }
        });
    }

    @Override
    public FilmsPresenter createPresenter() {
        return new FilmsPresenterImpl(MovieApp.provideMovieRepository());
    }

    @NonNull
    @Override
    public FilmsViewState createViewState() {
        return new FilmsStateImpl();
    }

    @Override
    public void onNewViewStateInstance() {
        presenter.onNewViewStateInstance();
    }

    @Override
    public void showLoading() {
        viewState.setShowLoadingState();
        pbLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        pbLoading.setVisibility(View.GONE);
    }

    @Override
    public void showLoadingMorePages() {
        viewState.setShowLoadingMorePagesState();
        pbLoadingContent.setVisibility(View.VISIBLE);
    }

    @Override
    public void showFilms(List<MovieDTO> films) {
        viewState.setShowFilmsState();
        adapter.setData(films);
        rvFilms.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingMorePages() {
        pbLoadingContent.setVisibility(View.GONE);
    }

    @Override
    public void restoreFilmsState() {
        presenter.restoreFilmsState();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleBottomNavigationEvent(BottomNavigationItemSelected event) {
        presenter.onNavigationItemSelected(event.getEvent());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleFabSheetEvent(FabSheetEvent event){
        presenter.obFabSheetEvent(event.getViewId());
    }

    @Override
    public void setData(List<MovieDTO> films) {
        viewState.setShowFilmsState();
        adapter.addData(films);
    }

    @Override
    public void refreshScrollPosition() {
        layoutManager.scrollToPosition(AppConstants.DEFAULT_SCROLL_POSITION);
    }

    @Override
    public void showMovieInfoScreen() {
        EventBus.getDefault().post(new FabEvent(false));
        ViewUtils.replaceFragment(Objects.requireNonNull(getFragmentManager()),
                MovieInfoFragment.newInstance(), getString(R.string.tag_movie_info));
    }

    @Override
    public void showNoInternetConnectionView() {
        viewState.setStateNoInternetConnections();
        layoutNoInternetConnections.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideNoInternetConnectionView() {
        layoutNoInternetConnections.setVisibility(View.GONE);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        presenter.attachView(this);
    }

    @NonNull
    private FilmsAdapter createAdapter() {
        TypedValue typedValue = new TypedValue();
        getResources().getValue(R.dimen.rows_count, typedValue, true);
        float rowsCount = typedValue.getFloat();
        int actionBarHeight = Objects.requireNonNull(getActivity()).getTheme()
                .resolveAttribute(R.attr.actionBarSize, typedValue, true)
                ? TypedValue.complexToDimensionPixelSize(typedValue.data, getResources().getDisplayMetrics())
                : 0;
        int imageHeight = (int) ((getResources().getDisplayMetrics().heightPixels - actionBarHeight
                - ViewUtils.dpToPx(getResources().getInteger(R.integer.image_height))) / rowsCount);
        int columns = getResources().getInteger(R.integer.columns_count);
        int imageWidth = (getResources().getDisplayMetrics().widthPixels
                - ViewUtils.dpToPx(getResources().getInteger(R.integer.image_width))) / columns;
        return new FilmsAdapter(imageHeight, imageWidth);
    }

    @OnClick(R.id.fragment_films_btn_retry)
    public void onRetryBtnClick() {
        viewState.setStateDefault();
        layoutNoInternetConnections.setVisibility(View.GONE);
        presenter.restoreFilmsState();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}