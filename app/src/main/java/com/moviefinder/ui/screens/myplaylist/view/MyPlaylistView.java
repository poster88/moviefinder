package com.moviefinder.ui.screens.myplaylist.view;


import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.moviefinder.data.mapper.MovieDTO;

import java.util.List;

public interface MyPlaylistView extends MvpView {
    void showAlertDialog(int titleId, int icon, boolean isCancelable, int posBtnTitleId, int negBtnTitleId);

    void showPlaylist(List<MovieDTO> list);

    void showNoResultView();

    void restoreListState();

    void removeFragment();

    void updateAdapter(int position);

    void updateAdapter();

    void removeAllItems();

    void showMovieInfoScreen();

    void showTVShowsScreen();
}
