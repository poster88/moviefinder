package com.moviefinder.ui.screens.tvshow;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RatingBar;
import android.widget.TextView;

import com.moviefinder.R;
import com.moviefinder.data.mapper.MovieDTO;
import com.moviefinder.ui.views.OnItemClickListener;
import com.moviefinder.ui.views.OnMenuItemClickListener;
import com.moviefinder.utils.ViewUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TVShowsAdapter extends RecyclerView.Adapter<TVShowsAdapter.TVShowsViewHolder> {

    private List<MovieDTO> data = new ArrayList<>();
    private OnItemClickListener itemClickListener;
    private OnMenuItemClickListener menuItemClickListener;
    private PopupMenu popupMenu;
    private int imageHeight;
    private int imageWidth;

    public TVShowsAdapter(int imageHeight, int imageWidth) {
        this.imageHeight = imageHeight;
        this.imageWidth = imageWidth;
    }

    @NonNull
    @Override
    public TVShowsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_films, parent, false);
        setupImageViewParams(view);
        return new TVShowsViewHolder(view);
    }

    private void setupImageViewParams(View view) {
        ImageView imageView = view.findViewById(R.id.item_films_img_poster);
        ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
        layoutParams.height = imageHeight;
        layoutParams.width = imageWidth;
        imageView.requestLayout();
    }

    public void setItemClickListener(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public void setMenuItemClickListener(OnMenuItemClickListener menuItemClickListener) {
        this.menuItemClickListener = menuItemClickListener;
    }

    @Override
    public void onBindViewHolder(@NonNull TVShowsViewHolder h, int position) {
        MovieDTO model = data.get(position);
        ViewUtils.loadImage(h.imgPoster, model.getPosterPath(), R.drawable.ic_square_placeholder_light_gray);
        h.textTitle.setText(model.getTitle());
        h.textGenres.setText(model.getStringGenres());
        h.rbVoteAverage.setRating(model.getFormattedVoteAverage());
        h.itemView.setOnClickListener(v ->
                itemClickListener.onItemClicked(model.getId(), model.getMediaType()));
        h.imgMenu.setOnClickListener(v -> {
            popupMenu = new PopupMenu(v.getContext(), v);
            popupMenu.inflate(R.menu.menu_context_movie);
            popupMenu.show();
            popupMenu.setOnMenuItemClickListener(item -> {
                menuItemClickListener.onMenuItemClicked(item.getItemId(), model.getId(), position);
                return true;
            });
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void addData(List<MovieDTO> models) {
        int currentSize = data.size();
        data.addAll(models);
        notifyItemRangeInserted(currentSize, currentSize - 1);
    }

    public void setData(List<MovieDTO> movies) {
        data.clear();
        data.addAll(movies);
        notifyDataSetChanged();
    }

    class TVShowsViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_films_img_poster) ImageView imgPoster;
        @BindView(R.id.item_films_text_title) TextView textTitle;
        @BindView(R.id.item_films_text_genre) TextView textGenres;
        @BindView(R.id.item_films_rb_rating) RatingBar rbVoteAverage;
        @BindView(R.id.item_films_img_btn_menu) ImageView imgMenu;

        TVShowsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}