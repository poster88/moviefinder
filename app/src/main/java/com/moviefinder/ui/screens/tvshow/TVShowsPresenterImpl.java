package com.moviefinder.ui.screens.tvshow;


import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.moviefinder.R;
import com.moviefinder.data.repository.MovieRepository;
import com.moviefinder.events.MessageEvent;
import com.moviefinder.ui.screens.tvshow.view.TVShowsView;
import com.moviefinder.utils.AppConstants;
import com.moviefinder.utils.ServerException;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.moviefinder.utils.AppConstants.DEFAULT_PAGE;

public class TVShowsPresenterImpl extends MvpBasePresenter<TVShowsView> implements TVShowsPresenter {

    public static final String TAG = "TVShowsPresenterImpl";

    private MovieRepository repository;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private int pageActual = DEFAULT_PAGE;
    private int pageRated = DEFAULT_PAGE;
    private int pageInCinema = DEFAULT_PAGE;
    private int pageNowPlaying = DEFAULT_PAGE;
    private int filterGenreId = R.id.fab_sheet_genre_all;
    private int categoryId = R.id.action_actual;
    private boolean isContentLoading;

    public TVShowsPresenterImpl(MovieRepository repository) {
        this.repository = repository;
    }

    @Override
    public void onScrolled(int visibleItemCount, int totalItemCount, int lastVisibleItemPosition, boolean isScrollDown) {
        boolean isLoadMore = !isContentLoading && (visibleItemCount + lastVisibleItemPosition) >= totalItemCount
                && isScrollDown;
        if (isLoadMore) compositeDisposable.add(loadMoreContent(getPage(categoryId, false)));
    }

    private Disposable loadMoreContent(int page) {
        return repository.getMoreTVShows(++page, categoryId, filterGenreId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> {
                    isContentLoading = true;
                    ifViewAttached(TVShowsView::showLoadingMorePages);
                })
                .doFinally(() -> {
                    isContentLoading = false;
                    ifViewAttached(TVShowsView::hideLoadingMorePages);
                })
                .subscribe(movies -> {
                    getPage(categoryId, true);
                    ifViewAttached(view -> view.setData(movies));
                }, this::handleException);
    }

    private void handleException(Throwable throwable) {
        ServerException error = (ServerException) throwable;
        int errorId = R.string.error_default;
        if (error.getKind() == ServerException.Kind.NETWORK) {
            errorId = R.string.error_no_connection;
        } else if (error.getKind() == ServerException.Kind.HTTP) {
            errorId = R.string.error_bad_http_request;
        } else if (error.getKind() == ServerException.Kind.UNEXPECTED) {
            errorId = R.string.error_unexpected;
        }
        EventBus.getDefault().post(new MessageEvent(errorId));
    }

    @Override
    public void onNewViewStateInstance() {
        compositeDisposable.add(showTVShows());
    }

    private Disposable showTVShows() {
        return repository.getTVShows(getPage(categoryId, false), categoryId, filterGenreId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> ifViewAttached(TVShowsView::showLoading))
                .doAfterTerminate(() -> ifViewAttached(TVShowsView::hideLoading))
                .onErrorReturn(throwable -> {
                    ServerException error = (ServerException) throwable;
                    if (error.getKind() == ServerException.Kind.NETWORK) {
                        ifViewAttached(TVShowsView::showNoInternetConnectionView);
                    }
                    return new ArrayList<>();
                })
                .subscribe(shows -> ifViewAttached(view -> view.showTVShows(shows)));
    }

    @Override
    public void onNavigationItemSelected(int itemId) {
        categoryId = itemId;
        compositeDisposable.add(showTVShows());
        ifViewAttached(TVShowsView::refreshScrollPosition);
        ifViewAttached(TVShowsView::hideNoInternetConnectionView);
    }

    @Override
    public void obFabSheetEvent(int viewId) {
        if (filterGenreId != viewId) {
            filterGenreId = viewId;
            compositeDisposable.add(showTVShows());
        }
    }

    @Override
    public void restoreTVShowsState() {
        compositeDisposable.add(showTVShows());
    }

    @Override
    public void onItemClicked(int itemId) {
        repository.setLastTVShowDetails(categoryId, itemId);
        ifViewAttached(TVShowsView::showTVShowInfoScreen);
    }

    @Override
    public void onMenuItemClicked(int menuItemId, int itemId, int position) {
        if (menuItemId == R.id.action_add_to_favorite) {
            repository.setLastTVShowDetails(categoryId, itemId);
            compositeDisposable.add(addToItemToFavorite(String.valueOf(itemId), AppConstants.TYPE_TV_SHOW));
        }
    }

    private Disposable addToItemToFavorite(String itemId, String type) {
        return repository.addToFavorites(itemId, type)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(isItemAdded -> {
                    if (isItemAdded) {
                        EventBus.getDefault().post(new MessageEvent(R.string.add_to_favorite));
                    } else {
                        EventBus.getDefault().post(new MessageEvent(R.string.fail_add_to_favorite));
                    }
                });
    }

    private int getPage(int categoryId, boolean isIncrease) {
        if (categoryId == R.id.action_actual) {
            return isIncrease ? ++pageActual : pageActual;
        } else if (categoryId == R.id.action_rated) {
            return isIncrease ? ++pageRated : pageRated;
        } else if (categoryId == R.id.action_in_release) {
            return isIncrease ? ++pageInCinema : pageInCinema;
        } else {
            return isIncrease ? ++pageNowPlaying : pageNowPlaying;
        }
    }
}