package com.moviefinder.ui.screens.search.view;

import com.hannesdorfmann.mosby3.mvp.viewstate.ViewState;


public interface SearchViewState extends ViewState<SearchView> {

    void setStateFadeDimBackground(int value);

    void setStateShowSearchResults();

    void setStateShowResultContainer();

    void setStateHideContainer();

    void setStateNoResults();
}
