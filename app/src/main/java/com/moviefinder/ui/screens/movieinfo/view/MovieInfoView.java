package com.moviefinder.ui.screens.movieinfo.view;

import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.moviefinder.data.mapper.MovieDTO;

public interface MovieInfoView extends MvpView {

    void showMovieInfo(MovieDTO movie);

    void restoreMovieState();

    void removeFragment();
}
