package com.moviefinder.ui.screens.searchinfo.view;

import com.moviefinder.utils.AppConstants;

public class SearchInfoViewStateImpl implements SearchInfoViewState {

    public static final String TAG = "SearchInfoViewStateImpl";

    private int currentState;

    @Override
    public void apply(SearchInfoView view, boolean retained) {
        if (currentState == AppConstants.STATE_SHOW) {
            view.restoreSearchInfoState();
        }
    }

    @Override
    public void setShowSearchInfo() {
        currentState = AppConstants.STATE_SHOW;
    }
}