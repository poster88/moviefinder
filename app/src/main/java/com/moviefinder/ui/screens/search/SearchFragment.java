package com.moviefinder.ui.screens.search;

import android.animation.ValueAnimator;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.moviefinder.MovieApp;
import com.moviefinder.R;
import com.moviefinder.data.mapper.MovieDTO;
import com.moviefinder.events.FabEvent;
import com.moviefinder.events.OffsetChangeEvent;
import com.moviefinder.ui.BaseFragment;
import com.moviefinder.ui.screens.myplaylist.MyPlaylistFragment;
import com.moviefinder.ui.screens.search.view.SearchView;
import com.moviefinder.ui.screens.search.view.SearchViewState;
import com.moviefinder.ui.screens.search.view.SearchViewStateImpl;
import com.moviefinder.ui.screens.searchinfo.SearchInfoFragment;
import com.moviefinder.utils.AppConstants;
import com.moviefinder.utils.ViewUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchFragment extends BaseFragment<SearchView, SearchPresenter, SearchViewState> implements
        SearchView, FloatingSearchView.OnFocusChangeListener, ValueAnimator.AnimatorUpdateListener,
        FloatingSearchView.OnSearchListener {

    public static final String TAG = "SearchFragment";
    public static SearchFragment newInstance() {
        Bundle args = new Bundle();
        SearchFragment fragment = new SearchFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.fragment_search_floating_search_view) FloatingSearchView floatingSearchView;
    @BindView(R.id.fragment_search_rv_results) RecyclerView rvSearchResults;
    @BindView(R.id.fragment_search_text_not_found) TextView textNoResults;
    @BindView(R.id.fragment_search_no_result_container) LinearLayout noResultsLayout;
    @BindView(R.id.fragment_search_components_container) LinearLayout searchComponentsLayout;
    @BindView(R.id.fragment_search_dim_background) FrameLayout dimSearchBackGround;

    private ColorDrawable dimDrawable;
    private LinearLayoutManager layoutManager;
    private SearchAdapter adapter;

    @NonNull
    @Override
    public SearchPresenter createPresenter() {
        return new SearchPresenterImpl(MovieApp.provideMovieRepository());
    }

    @NonNull
    @Override
    public SearchViewState createViewState() {
        return new SearchViewStateImpl();
    }

    @Override
    public void onNewViewStateInstance() {
        presenter.onNewViewStateInstance();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.bind(this, view);
        setupSearchBar();
        setupDim();
        initComponents();
        setupRecycleView();
        return view;
    }

    public void setupSearchBar() {
        floatingSearchView.setOnFocusChangeListener(this);
        floatingSearchView.setOnSearchListener(this);
        floatingSearchView.setOnBindSuggestionCallback((suggestionView, leftIcon, textView, item, itemPosition) ->
                bindSuggestion((QuerySuggestion) item, leftIcon));
        floatingSearchView.setOnQueryChangeListener((oldQuery, newQuery) ->
                presenter.onQueryChange(newQuery));
        floatingSearchView.setOnMenuItemClickListener(item ->
                presenter.onActionItemSelected(item.getItemId()));
        floatingSearchView.setCloseSearchOnKeyboardDismiss(true);
    }

    private void setupDim() {
        dimDrawable = new ColorDrawable(Color.BLACK);
        dimDrawable.setAlpha(getResources().getInteger(R.integer.default_alpha));
        dimSearchBackGround.setBackground(dimDrawable);
    }

    private void initComponents() {
        layoutManager = new LinearLayoutManager(getContext());
        adapter = new SearchAdapter();
        adapter.setOnItemClickListener((itemId, type) -> presenter.onItemClicked(itemId, type));
        adapter.setOnMenuItemClickListener((menuItem, itemId, type) ->
                presenter.onMenuItemClicked(menuItem, itemId, type));
    }

    private void setupRecycleView() {
        rvSearchResults.setLayoutManager(layoutManager);
        rvSearchResults.setAdapter(adapter);
        rvSearchResults.addOnScrollListener(new OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition();
                presenter.onScrolled(lastVisibleItemPosition, layoutManager.getItemCount(), dy > 0);
            }
        });
    }

    @Override
    public void onSuggestionClicked(SearchSuggestion searchSuggestion) {
        presenter.onSuggestionClicked((QuerySuggestion) searchSuggestion);
        floatingSearchView.setSearchText(searchSuggestion.getBody());
        floatingSearchView.setSearchFocused(false);
        noResultsLayout.setVisibility(View.GONE);
    }

    @Override
    public void onSearchAction(String currentQuery) {
        presenter.onSearchAction(currentQuery);
        noResultsLayout.setVisibility(View.GONE);
        layoutManager.scrollToPosition(AppConstants.DEFAULT_SCROLL_POSITION);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        presenter.attachView(this);
    }

    @Override
    public void onFocus() {
        presenter.onSearchFocus(isVisible(searchComponentsLayout));
    }

    @Override
    public void onFocusCleared() {
        presenter.onSearchFocusCleared(isVisible(searchComponentsLayout));
    }

    @Override
    public void fadeDimBackground(int from, int to) {
        viewState.setStateFadeDimBackground(from);
        ValueAnimator anim = ValueAnimator.ofInt(from, to);
        anim.setDuration(AppConstants.ANIMATION_DURATION);
        anim.start();
        anim.addUpdateListener(this);
    }

    @Override
    public void clearSuggestions() {
        floatingSearchView.setSearchFocused(false);
    }

    @Override
    public void clearSearchView() {
        floatingSearchView.clearQuery();
    }

    @Override
    public void hideComponentsContainer() {
        viewState.setStateHideContainer();
        searchComponentsLayout.setVisibility(View.GONE);
        EventBus.getDefault().post(new FabEvent(true));
    }

    @Override
    public void swapSearchSuggestion(List<QuerySuggestion> suggestions) {
        if (floatingSearchView.isSearchBarFocused()) floatingSearchView.swapSuggestions(suggestions);
    }

    @Override
    public void showSearchResults(List<MovieDTO> searchResult) {
        viewState.setStateShowSearchResults();
        adapter.setData(searchResult);
        rvSearchResults.setVisibility(View.VISIBLE);
    }

    @Override
    public void restoreSearchResults() {
        presenter.restoreSearchResults();
    }

    @Override
    public void showNoResults() {
        viewState.setStateNoResults();
        rvSearchResults.setVisibility(View.GONE);
        noResultsLayout.setVisibility(View.VISIBLE);
        textNoResults.setText(String.format(getString(R.string.search_text_no_results),
                floatingSearchView.getQuery()));
    }

    @Override
    public void showMyPlayListScreen() {
        EventBus.getDefault().post(new FabEvent(false));
        ViewUtils.replaceFragment(Objects.requireNonNull(getFragmentManager()),
                MyPlaylistFragment.newInstance(), getString(R.string.tag_play_list));
    }

    @Override
    public void showSearchInfo() {
        EventBus.getDefault().post(new FabEvent(false));
        ViewUtils.replaceFragment(Objects.requireNonNull(getFragmentManager()),
                SearchInfoFragment.newInstance(), getString(R.string.tag_search_info));
    }

    @Override
    public void onBackPress() {
        if (isResultsVisible()) {
            clearSearchView();
            hideComponentsContainer();
        }
    }

    @Override
    public void showLoadingMorePages() {
        adapter.add(null);
    }

    @Override
    public void hideLoadingMorePages() {
        adapter.remove(adapter.getItemCount() - 1);
    }

    @Override
    public void setData(List<MovieDTO> searches) {
        viewState.setStateShowSearchResults();
        adapter.addAll(searches);
    }

    @Override
    public void showComponentContainer() {
        viewState.setStateShowResultContainer();
        searchComponentsLayout.setVisibility(View.VISIBLE);
        EventBus.getDefault().post(new FabEvent(false));
    }

    @Override
    public void onAnimationUpdate(ValueAnimator animation) {
        int value = (Integer) animation.getAnimatedValue();
        dimDrawable.setAlpha(value);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onOffsetChanged(OffsetChangeEvent event) {
        floatingSearchView.setTranslationY(event.getVerticalOffset());
    }

    private void bindSuggestion(QuerySuggestion suggestion, ImageView leftIcon) {
        float alpha = suggestion.getIsHistory() ? .36f : 0.0f;
        Drawable drawable = suggestion.getIsHistory()
                ? ResourcesCompat.getDrawable(getResources(), R.drawable.ic_history_black_24dp, null)
                : null;
        leftIcon.setAlpha(alpha);
        leftIcon.setImageDrawable(drawable);
    }

    public boolean isVisible(View view) {
        return view.getVisibility() == View.VISIBLE;
    }

    public boolean isResultsVisible() {
        return isVisible(searchComponentsLayout);
    }
}