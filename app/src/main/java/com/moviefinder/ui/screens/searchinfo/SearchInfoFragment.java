package com.moviefinder.ui.screens.searchinfo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.moviefinder.MovieApp;
import com.moviefinder.R;
import com.moviefinder.data.mapper.MovieDTO;
import com.moviefinder.events.FabEvent;
import com.moviefinder.ui.BaseFragment;
import com.moviefinder.ui.screens.searchinfo.view.SearchInfoView;
import com.moviefinder.ui.screens.searchinfo.view.SearchInfoViewState;
import com.moviefinder.ui.screens.searchinfo.view.SearchInfoViewStateImpl;
import com.moviefinder.utils.ViewUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.annotations.Nullable;

public class SearchInfoFragment extends BaseFragment<SearchInfoView, SearchInfoPresenter, SearchInfoViewState>
        implements SearchInfoView {

    public static SearchInfoFragment newInstance() {
        Bundle args = new Bundle();
        SearchInfoFragment fragment = new SearchInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.fragment_info_toolbar) Toolbar toolbar;
    @BindView(R.id.fragment_info_img_background) ImageView imgBackground;
    @BindView(R.id.fragment_info_poster) ImageView imgPoster;
    @BindView(R.id.fragment_info_text_title) TextView textTitle;
    @BindView(R.id.fragment_info_text_genres) TextView textGenres;
    @BindView(R.id.fragment_info_text_average) TextView textAverage;
    @BindView(R.id.fragment_info_text_popularity) TextView textPopularity;
    @BindView(R.id.fragment_info_text_review) TextView textReview;
    @BindView(R.id.fragment_info_load_components) LinearLayout loadComponentLayout;
    @BindView(R.id.fragment_info_pb_loading) ProgressBar pbLoadProgress;

    @Override
    public SearchInfoPresenter createPresenter() {
        return new SearchInfoPresenterImpl(MovieApp.provideMovieRepository());
    }

    @NonNull
    @Override
    public SearchInfoViewState createViewState() {
        return new SearchInfoViewStateImpl();
    }

    @Override
    public void onNewViewStateInstance() {
        presenter.onNewViewStateInstance();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_info, container, false);
        ButterKnife.bind(this, view);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(v -> presenter.onNavigationBtnClick());
        return view;
    }

    @Override
    public void restoreSearchInfoState() {
        presenter.restoreSearchInfoState();
    }

    @Override
    public void removeFragment() {
        EventBus.getDefault().post(new FabEvent(true));
        Objects.requireNonNull(getActivity()).onBackPressed();
    }

    @Override
    public void showSearchInfo(MovieDTO search) {
        viewState.setShowSearchInfo();
        ViewUtils.loadImage(imgBackground, search.getBackdropPath());
        ViewUtils.loadImage(imgPoster, search.getPosterPath());
        textTitle.setText(search.getFormattedTitle());
        textGenres.setText(search.getStringGenres());
        textAverage.setText(search.getStringVoteAverage());
        textPopularity.setText(String.valueOf(search.getVoteCount()));
        textReview.setText(search.getOverview());

    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        presenter.attachView(this);
    }
}
