package com.moviefinder.ui.screens.main;

import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.gordonwong.materialsheetfab.MaterialSheetFab;
import com.moviefinder.MovieApp;
import com.moviefinder.R;
import com.moviefinder.events.BottomNavigationItemSelected;
import com.moviefinder.events.ButtonClickEvent;
import com.moviefinder.events.FabEvent;
import com.moviefinder.events.FabSheetEvent;
import com.moviefinder.events.MessageEvent;
import com.moviefinder.events.OffsetChangeEvent;
import com.moviefinder.ui.BaseActivity;
import com.moviefinder.ui.screens.films.FilmsFragment;
import com.moviefinder.ui.screens.search.SearchFragment;
import com.moviefinder.ui.screens.main.view.MainView;
import com.moviefinder.ui.screens.main.view.MainViewState;
import com.moviefinder.ui.screens.main.view.MainViewStateImpl;
import com.moviefinder.ui.screens.review.ReviewFragment;
import com.moviefinder.ui.screens.tvshow.TVShowsFragment;
import com.moviefinder.ui.views.Fab;
import com.moviefinder.ui.views.OnPageChangeListener;
import com.moviefinder.utils.ViewUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity<MainView, MainPresenter, MainViewState>
        implements MainView, AppBarLayout.OnOffsetChangedListener {

    public static final String TAG = "MainActivity";

    @BindView(R.id.activity_main_app_bar_layout) AppBarLayout appBarLayout;
    @BindView(R.id.activity_main_tab_layout) TabLayout tabLayout;
    @BindView(R.id.activity_main_view_pager) ViewPager viewPager;
    @BindView(R.id.activity_main_bottom_nav) BottomNavigationView bottomNavigationView;
    @BindView(R.id.activity_main_fab) Fab fab;
    @BindView(R.id.activity_main_fab_sheet) View viewFabSheet;
    @BindView(R.id.activity_main_overlay) View viewOverlay;
    @BindView(R.id.main_activity_coordinator_layout) CoordinatorLayout viewContainer;

    private MaterialSheetFab materialSheetFab;

    @NonNull
    @Override
    public MainPresenter createPresenter() {
        return new MainPresenterImpl(MovieApp.provideMovieRepository());
    }

    @NonNull
    @Override
    public MainViewState createViewState() {
        return new MainViewStateImpl();
    }

    @Override
    public void onNewViewStateInstance() {
        changeDrawable(R.id.fab_sheet_genre_all, R.drawable.ic_crop_full_accent_dark_24dp);
        showSearchScreen();
    }

    private void showSearchScreen() {
        ViewUtils.addFragment(getSupportFragmentManager(), R.id.search_view_placeholder,
                SearchFragment.newInstance(), getString(R.string.tag_search_fragment));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        appBarLayout.addOnOffsetChangedListener(this);
        setupFab();
        setupViewPager();
        setupBottomNavigationView();
    }

    @Override
    public void setViewPagerCurrentItem(int position) {
        viewPager.setCurrentItem(position);
    }

    @Override
    public void changeDrawable(int viewId, int resId) {
        TextView view = findViewById(viewId);
        int defBounds = getResources().getInteger(R.integer.default_bounds);
        view.setCompoundDrawablesRelativeWithIntrinsicBounds(resId, defBounds, defBounds, defBounds);
    }

    @Override
    public void hideFab() {
        if (viewState != null) viewState.isFabVisible(false);
        fab.hide();
    }

    @Override
    public void showFab() {
        if (viewState != null) viewState.isFabVisible(true);
        fab.show();
    }

    @Override
    public void restoreFabState(boolean isVisible) {
        presenter.setupFabVisibility(isVisible);
    }

    @Override
    public void hideBottomNavigationView() {
        bottomNavigationView.setVisibility(View.GONE);
    }

    @Override
    public void showBottomNavigationView() {
        bottomNavigationView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        OffsetChangeEvent event = new OffsetChangeEvent(verticalOffset);
        EventBus.getDefault().post(event);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleOnClickEvent(ButtonClickEvent event) {
        presenter.onClickEvent(event.getBtnId());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleMessage(MessageEvent event) {
        presenter.handleMessage(event.getResId());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleFabVisibility(FabEvent event) {
        presenter.setupFabVisibility(event.isVisible());
    }

    @Override
    public void showMessage(int resId, int titleId) {
        Snackbar.make(viewContainer, getString(resId), Snackbar.LENGTH_INDEFINITE)
                .setAction(titleId, v -> presenter.onSnackBarClicked(resId))
                .show();
    }

    @Override
    public void showMessage(int resId) {
        Snackbar.make(viewContainer, getString(resId), Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.action_ok, v -> {})
                .show();
    }

    public void setupViewPager() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(ReviewFragment.newInstance(), getString(R.string.tab_main));
        adapter.addFragment(FilmsFragment.newInstance(), getString(R.string.tab_films));
        adapter.addFragment(TVShowsFragment.newInstance(), getString(R.string.tab_tv_shows));
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.addOnPageChangeListener(new OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                presenter.onViewPageSelected(position);
            }
        });
    }

    public void setupBottomNavigationView() {
        presenter.setupBottomNavigation();
        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            EventBus.getDefault().post(new BottomNavigationItemSelected(item.getItemId()));
            return true;
        });
    }

    public void setupFab() {
        int sheetColor = getResources().getColor(R.color.primaryTextColor);
        int fabColor = getResources().getColor(R.color.colorAccent);
        materialSheetFab = new MaterialSheetFab<>(fab, viewFabSheet, viewOverlay, sheetColor, fabColor);
    }

    @OnClick({R.id.fab_sheet_genre_action, R.id.fab_sheet_genre_western, R.id.fab_sheet_genre_comedy,
            R.id.fab_sheet_genre_criminal, R.id.fab_sheet_genre_all})
    public void onFabSheetItemSelected(View view) {
        viewState.saveFabSheetItemSelectedState(view.getId());
        presenter.onFabSheetItemSelected(view.getId());
        EventBus.getDefault().post(new FabSheetEvent(view.getId()));
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            presenter.setupFabVisibility(true);
            getSupportFragmentManager().popBackStack();
            return;
        }
        if (materialSheetFab.isSheetVisible()) {
            materialSheetFab.hideSheet();
            return;
        }
        SearchFragment searchFragment = (SearchFragment) getSupportFragmentManager().findFragmentById(R.id.search_view_placeholder);
        if (searchFragment.isResultsVisible()) {
            searchFragment.onBackPress();
            return;
        }
        super.onBackPressed();
    }
}