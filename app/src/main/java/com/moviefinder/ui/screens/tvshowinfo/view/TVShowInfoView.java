package com.moviefinder.ui.screens.tvshowinfo.view;

import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.moviefinder.data.mapper.MovieDTO;
import com.moviefinder.data.model.content.TVShow;

public interface TVShowInfoView extends MvpView {
    void showMovieInfo(MovieDTO tvShow);

    void restoreMovieState();

    void removeFragment();
}
