package com.moviefinder.ui.screens.myplaylist;


import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.moviefinder.R;
import com.moviefinder.data.repository.MovieRepository;
import com.moviefinder.events.ButtonClickEvent;
import com.moviefinder.events.FabEvent;
import com.moviefinder.events.MessageEvent;
import com.moviefinder.ui.screens.myplaylist.view.MyPlaylistView;
import com.moviefinder.utils.AppConstants;

import org.greenrobot.eventbus.EventBus;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MyPlaylistPresenterImpl extends MvpBasePresenter<MyPlaylistView> implements
        MyPlaylistPresenter {

    public static final String TAG = "MyPlaylistPresenterImpl";

    private MovieRepository repository;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private int categoryId = R.id.action_actual;

    public MyPlaylistPresenterImpl(MovieRepository repository) {
        this.repository = repository;
    }

    @Override
    public void onBtnGoMainClick(int btnId) {
        EventBus.getDefault().post(new ButtonClickEvent(btnId));
        onNavigationBtnClick();
    }

    @Override
    public void onNavigationBtnClick() {
        ifViewAttached(MyPlaylistView::removeFragment);
        EventBus.getDefault().post(new FabEvent(true));
    }

    @Override
    public void onToolBarMenuItemClick(int itemId, int count) {
        if (itemId == R.id.action_delete_all) {
            if (count > 0) {
                ifViewAttached(view -> view.showAlertDialog(R.string.title_remove, R.drawable.ic_delete_black_24dp,
                        false, R.string.title_ok, R.string.cancel_remove));
            } else {
                EventBus.getDefault().post(new MessageEvent(R.string.no_items));
            }
        }
    }

    @Override
    public void onNewViewStateInstance() {
        compositeDisposable.add(initPlaylistItems());
    }

    @Override
    public void onItemClicked(int itemId, String type) {
        if (type.equals(AppConstants.TYPE_MOVIE)) {
            repository.setLastMovieDetails(categoryId, itemId);
            ifViewAttached(MyPlaylistView::showMovieInfoScreen);
        } else {
            repository.setLastTVShowDetails(categoryId, itemId);
            ifViewAttached(MyPlaylistView::showTVShowsScreen);
        }
    }

    @Override
    public void onMenuItemClicked(int menuId, int itemId, int position) {
        if (menuId == R.id.action_remove_from_favorite) {
            compositeDisposable.add(removeItemFromFavorite(String.valueOf(itemId), position));
        }
    }

    private Disposable removeItemFromFavorite(String itemId, int position) {
        return repository.removeItemFromFavorite(itemId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> ifViewAttached(view -> view.updateAdapter(position)));
    }

    @Override
    public void onAlertPositiveBtnClick() {
        compositeDisposable.add(removePlaylistItems());
    }

    private Disposable removePlaylistItems() {
        return repository.removeAllItemsFromFavorite()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(() -> compositeDisposable.add(initPlaylistItems()))
                .subscribe(() -> ifViewAttached(MyPlaylistView::removeAllItems));
    }

    @Override
    public void restoreList() {
        compositeDisposable.add(initPlaylistItems());
    }

    private Disposable initPlaylistItems() {
        return repository.getPlayListItems()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(movieDTOList -> {
                    if (!movieDTOList.isEmpty()) {
                        ifViewAttached(view -> view.showPlaylist(movieDTOList));
                    } else {
                        ifViewAttached(MyPlaylistView::showNoResultView);
                    }
                });
    }
}