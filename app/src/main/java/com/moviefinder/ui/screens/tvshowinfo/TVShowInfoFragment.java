package com.moviefinder.ui.screens.tvshowinfo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.moviefinder.MovieApp;
import com.moviefinder.R;
import com.moviefinder.data.mapper.MovieDTO;
import com.moviefinder.ui.BaseFragment;
import com.moviefinder.ui.screens.tvshowinfo.view.TVShowInfoView;
import com.moviefinder.ui.screens.tvshowinfo.view.TVShowInfoViewState;
import com.moviefinder.ui.screens.tvshowinfo.view.TVShowInfoViewStateImpl;
import com.moviefinder.utils.ViewUtils;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TVShowInfoFragment extends BaseFragment<TVShowInfoView, TVShowInfoPresenter, TVShowInfoViewState>
        implements TVShowInfoView {

    public static final String TAG = "TVShowInfoFragment";

    public static TVShowInfoFragment newInstance() {
        Bundle args = new Bundle();
        TVShowInfoFragment fragment = new TVShowInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.fragment_info_toolbar) Toolbar toolbar;
    @BindView(R.id.fragment_info_img_background) ImageView imgBackground;
    @BindView(R.id.fragment_info_poster) ImageView imgPoster;
    @BindView(R.id.fragment_info_text_title) TextView textTitle;
    @BindView(R.id.fragment_info_text_genres) TextView textGenres;
    @BindView(R.id.fragment_info_text_average) TextView textAverage;
    @BindView(R.id.fragment_info_text_popularity) TextView textPopularity;
    @BindView(R.id.fragment_info_text_review) TextView textReview;
    @BindView(R.id.fragment_info_load_components) LinearLayout loadComponentLayout;
    @BindView(R.id.fragment_info_pb_loading) ProgressBar pbLoadProgress;


    @Override
    public TVShowInfoPresenter createPresenter() {
        return new TVShowInfoPresenterImpl(MovieApp.provideMovieRepository());
    }

    @NonNull
    @Override
    public TVShowInfoViewState createViewState() {
        return new TVShowInfoViewStateImpl();
    }

    @Override
    public void onNewViewStateInstance() {
        presenter.onNewViewStateInstance();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info, container, false);
        ButterKnife.bind(this, view);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(v -> presenter.onNavigationBtnClick());
        return view;
    }

    @Override
    public void showMovieInfo(MovieDTO tvShow) {
        viewState.showMovieInfo();
        ViewUtils.loadImage(imgBackground, tvShow.getBackdropPath());
        ViewUtils.loadImage(imgPoster, tvShow.getPosterPath());
        textTitle.setText(tvShow.getFormattedTitle());
        textGenres.setText(tvShow.getStringGenres());
        textAverage.setText(tvShow.getStringVoteAverage());
        textPopularity.setText(String.valueOf(tvShow.getVoteCount()));
        textReview.setText(tvShow.getOverview());
    }

    @Override
    public void restoreMovieState() {
        presenter.restoreMovieInfoState();
    }

    @Override
    public void removeFragment() {
        Objects.requireNonNull(getActivity()).onBackPressed();
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        presenter.attachView(this);
    }
}