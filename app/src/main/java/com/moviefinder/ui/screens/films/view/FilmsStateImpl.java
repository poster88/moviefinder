package com.moviefinder.ui.screens.films.view;


public class FilmsStateImpl implements FilmsViewState {

    public static final String TAG = "FilmsStateImpl";

    private final int STATE_DEFAULT = 0;
    private final int STATE_IS_LOADING = 1;
    private final int STATE_SHOW_FILMS = 2;
    private final int STATE_LOADING_MORE_PAGES = 3;
    private final int STATE_ERROR_NO_INTERNET = -1;
    private int filmsViewState;

    @Override
    public void apply(FilmsView view, boolean retained) {
        if (filmsViewState == STATE_SHOW_FILMS) {
            view.restoreFilmsState();
        } else if (filmsViewState == STATE_IS_LOADING) {
            view.showLoading();
        } else if (filmsViewState == STATE_LOADING_MORE_PAGES) {
            view.restoreFilmsState();
            view.showLoadingMorePages();
        } else if (filmsViewState == STATE_ERROR_NO_INTERNET) {
            view.showNoInternetConnectionView();
        }
    }

    @Override
    public void setShowLoadingState() {
        filmsViewState = STATE_IS_LOADING;
    }

    @Override
    public void setShowLoadingMorePagesState() {
        filmsViewState = STATE_LOADING_MORE_PAGES;
    }

    @Override
    public void setShowFilmsState() {
        filmsViewState = STATE_SHOW_FILMS;
    }

    @Override
    public void setStateNoInternetConnections() {
        filmsViewState = STATE_ERROR_NO_INTERNET;
    }

    @Override
    public void setStateDefault() {
        filmsViewState = STATE_DEFAULT;
    }
}