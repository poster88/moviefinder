package com.moviefinder.ui.screens.movieinfo;


import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.moviefinder.data.mapper.MovieDTO;
import com.moviefinder.data.repository.MovieRepository;
import com.moviefinder.ui.screens.movieinfo.view.MovieInfoView;


public class MovieInfoPresenterImpl extends MvpBasePresenter<MovieInfoView> implements MovieInfoPresenter {

    public static final String TAG = "TVShowInfoPresenterImpl";

    private MovieRepository repository;

    public MovieInfoPresenterImpl(MovieRepository repository) {
        this.repository = repository;
    }

    @Override
    public void onNavigationBtnClick() {
        ifViewAttached(MovieInfoView::removeFragment);
    }

    @Override
    public void onNewViewStateInstance() {
        showMovieInfo();
    }

    @Override
    public void restoreMovieInfoState() {
        showMovieInfo();
    }

    private void showMovieInfo() {
        MovieDTO movie = repository.getMovieInfo();
        ifViewAttached(view -> view.showMovieInfo(movie));
    }
}