package com.moviefinder.ui.screens.movieinfo;


import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.moviefinder.ui.screens.movieinfo.view.MovieInfoView;

public interface MovieInfoPresenter extends MvpPresenter<MovieInfoView> {

    void onNavigationBtnClick();

    void onNewViewStateInstance();

    void restoreMovieInfoState();
}
