package com.moviefinder.ui.screens.myplaylist;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.moviefinder.ui.screens.myplaylist.view.MyPlaylistView;

public interface MyPlaylistPresenter extends MvpPresenter<MyPlaylistView> {
    void onBtnGoMainClick(int btnId);

    void onNavigationBtnClick();

    void onToolBarMenuItemClick(int itemId, int count);

    void onNewViewStateInstance();

    void onItemClicked(int itemId, String type);

    void onMenuItemClicked(int menuId, int itemId, int position);

    void onAlertPositiveBtnClick();

    void restoreList();
}
