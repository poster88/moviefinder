package com.moviefinder.ui.screens.review;


import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.moviefinder.ui.screens.review.view.ReviewView;

public interface ReviewPresenter extends MvpPresenter<ReviewView> {

    void onMovieItemClicked(int itemId, String type);

    void onMovieMenuItemClicked(int menuItemId, int itemId, int position);

    void onTVShowItemClicked(int itemId, String type);

    void onTVShowMenuItemClicked(int menuItemId, int itemId, int position);

    void onNewViewStateInstance();

    void restoreMovies();

    void restoreTVShows();

    void handleConnectionEvent(boolean connection);
}