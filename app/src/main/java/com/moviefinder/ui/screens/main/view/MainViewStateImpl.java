package com.moviefinder.ui.screens.main.view;

import com.moviefinder.R;

public class MainViewStateImpl implements MainViewState {

    public static final String TAG = "MainViewStateImpl";

    private int fabSheetItemSelectedId = R.id.fab_sheet_genre_all;
    private boolean isFabVisible;

    @Override
    public void apply(MainView view, boolean retained) {
        view.changeDrawable(fabSheetItemSelectedId, R.drawable.ic_crop_full_accent_dark_24dp);
        view.restoreFabState(isFabVisible);
    }

    @Override
    public void saveFabSheetItemSelectedState(int viewId) {
        fabSheetItemSelectedId = viewId;
    }

    @Override
    public void isFabVisible(boolean isVisible) {
        isFabVisible = isVisible;
    }
}