package com.moviefinder.ui.screens.tvshow.view;


public class TVShowsStateImpl implements TVShowsState {

    public static final String TAG = "TVShowsStateImpl";

    private final int STATE_DEFAULT = 0;
    private final int STATE_IS_LOADING = 1;
    private final int STATE_SHOW_TV_SHOWS = 2;
    private final int STATE_LOADING_MORE_PAGES = 3;
    private final int STATE_ERROR_NO_INTERNET = -1;
    private int tvShowsViewState;

    @Override
    public void apply(TVShowsView view, boolean retained) {
        if (tvShowsViewState == STATE_SHOW_TV_SHOWS) {
            view.restoreTVShowsState();
        } else if (tvShowsViewState == STATE_IS_LOADING) {
            view.showLoading();
        } else if (tvShowsViewState == STATE_LOADING_MORE_PAGES) {
            view.restoreTVShowsState();
            view.showLoadingMorePages();
        } else if (tvShowsViewState == STATE_ERROR_NO_INTERNET) {
            view.showNoInternetConnectionView();
        }
    }

    @Override
    public void setStateDefault() {
        tvShowsViewState = STATE_DEFAULT;
    }

    @Override
    public void setShowLoadingState() {
        tvShowsViewState = STATE_IS_LOADING;
    }

    @Override
    public void setShowLoadingMorePagesState() {
        tvShowsViewState = STATE_LOADING_MORE_PAGES;
    }

    @Override
    public void setShowTVShowsState() {
        tvShowsViewState = STATE_SHOW_TV_SHOWS;
    }

    @Override
    public void setStateNoInternetConnections() {
        tvShowsViewState = STATE_ERROR_NO_INTERNET;
    }
}