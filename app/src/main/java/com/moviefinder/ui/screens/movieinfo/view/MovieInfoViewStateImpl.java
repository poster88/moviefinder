package com.moviefinder.ui.screens.movieinfo.view;

import com.hannesdorfmann.mosby3.mvp.viewstate.ViewState;
import static com.moviefinder.utils.AppConstants.STATE_SHOW;

public class MovieInfoViewStateImpl implements ViewState<MovieInfoView>, MovieInfoViewState {

    private int currentState;

    @Override
    public void apply(MovieInfoView view, boolean retained) {
        if (currentState == STATE_SHOW) {
            view.restoreMovieState();
        }
    }

    @Override
    public void showMovieInfo() {
        currentState = STATE_SHOW;
    }
}