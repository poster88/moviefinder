package com.moviefinder.ui.screens.actors;

import com.moviefinder.data.repository.MovieRepository;

class ActorsPresenterImpl implements ActorsPresenter {

    public static final String TAG = ActorsPresenterImpl.class.getName();

    private ActorsView view;
    private MovieRepository repository;

    public ActorsPresenterImpl(ActorsView view, MovieRepository repository) {
        this.view = view;
        this.repository = repository;
    }
}