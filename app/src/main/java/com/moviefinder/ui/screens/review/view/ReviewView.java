package com.moviefinder.ui.screens.review.view;


import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.moviefinder.data.mapper.MovieDTO;

import java.util.List;

public interface ReviewView extends MvpView {

    void showMovies(List<MovieDTO> movies);

    void showTVShows(List<MovieDTO> tvShows);

    void showMoviesLoading();

    void hideMoviesLoading();

    void restoreMoviesState();

    void showError(int errorResId);

    void showMovieInfoScreen();

    void showTVShowsLoading();

    void hideTVShowLoading();

    void restoreTVShowsState();

    void showTVShowsScreen();
}