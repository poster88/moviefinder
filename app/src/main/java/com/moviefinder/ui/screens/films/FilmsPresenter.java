package com.moviefinder.ui.screens.films;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.moviefinder.ui.screens.films.view.FilmsView;

public interface FilmsPresenter extends MvpPresenter<FilmsView> {

    void onNewViewStateInstance();

    void restoreFilmsState();

    void onScrolled(int visibleItemCount, int totalItemCount, int lastVisibleItemPosition, boolean isScrollDown);

    void onNavigationItemSelected(int itemId);

    void obFabSheetEvent(int viewId);

    void onItemClicked(int itemId);

    void onMenuItemClicked(int menuItemId, int itemId, int position);
}