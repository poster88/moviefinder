package com.moviefinder.ui.screens.movieinfo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.moviefinder.MovieApp;
import com.moviefinder.R;
import com.moviefinder.data.mapper.MovieDTO;
import com.moviefinder.events.FabEvent;
import com.moviefinder.ui.BaseFragment;
import com.moviefinder.ui.screens.movieinfo.view.MovieInfoView;
import com.moviefinder.ui.screens.movieinfo.view.MovieInfoViewState;
import com.moviefinder.ui.screens.movieinfo.view.MovieInfoViewStateImpl;
import com.moviefinder.utils.ViewUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieInfoFragment extends BaseFragment<MovieInfoView, MovieInfoPresenter, MovieInfoViewState>
        implements MovieInfoView {

    public static final String TAG = "MovieInfoFragment";

    public static MovieInfoFragment newInstance() {
        Bundle args = new Bundle();
        MovieInfoFragment fragment = new MovieInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.fragment_info_toolbar) Toolbar toolbar;
    @BindView(R.id.fragment_info_img_background) ImageView imgBackground;
    @BindView(R.id.fragment_info_poster) ImageView imgPoster;
    @BindView(R.id.fragment_info_text_title) TextView textTitle;
    @BindView(R.id.fragment_info_text_genres) TextView textGenres;
    @BindView(R.id.fragment_info_text_average) TextView textAverage;
    @BindView(R.id.fragment_info_text_popularity) TextView textPopularity;
    @BindView(R.id.fragment_info_text_review) TextView textReview;
    @BindView(R.id.fragment_info_load_components) LinearLayout loadComponentLayout;
    @BindView(R.id.fragment_info_pb_loading) ProgressBar pbLoadProgress;


    @Override
    public MovieInfoPresenter createPresenter() {
        return new MovieInfoPresenterImpl(MovieApp.provideMovieRepository());
    }

    @NonNull
    @Override
    public MovieInfoViewState createViewState() {
        return new MovieInfoViewStateImpl();
    }

    @Override
    public void onNewViewStateInstance() {
        presenter.onNewViewStateInstance();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_info, container, false);
        ButterKnife.bind(this, view);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(v -> presenter.onNavigationBtnClick());
        return view;
    }

    @Override
    public void showMovieInfo(MovieDTO movie) {
        viewState.showMovieInfo();
        ViewUtils.loadImage(imgBackground, movie.getBackdropPath());
        ViewUtils.loadImage(imgPoster, movie.getPosterPath());
        textTitle.setText(movie.getFormattedTitle());
        textGenres.setText(movie.getStringGenres());
        textAverage.setText(movie.getStringVoteAverage());
        textPopularity.setText(String.valueOf(movie.getVoteCount()));
        textReview.setText(movie.getOverview());
    }

    @Override
    public void restoreMovieState() {
        presenter.restoreMovieInfoState();
    }

    @Override
    public void removeFragment() {
        Objects.requireNonNull(getActivity()).onBackPressed();
        EventBus.getDefault().post(new FabEvent(true));
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        presenter.attachView(this);
    }
}