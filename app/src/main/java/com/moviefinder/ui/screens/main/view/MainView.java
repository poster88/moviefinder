package com.moviefinder.ui.screens.main.view;

import com.hannesdorfmann.mosby3.mvp.MvpView;

public interface MainView extends MvpView {

    void showMessage(int resId, int titleId);

    void showMessage(int resId);

    void setViewPagerCurrentItem(int position);

    void changeDrawable(int viewId, int resId);

    void hideFab();

    void showFab();

    void restoreFabState(boolean isVisible);

    void hideBottomNavigationView();

    void showBottomNavigationView();
}