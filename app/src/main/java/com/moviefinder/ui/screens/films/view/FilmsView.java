package com.moviefinder.ui.screens.films.view;

import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.moviefinder.data.mapper.MovieDTO;

import java.util.List;

public interface FilmsView extends MvpView {

    void showLoading();

    void showLoadingMorePages();

    void showFilms(List<MovieDTO> films);

    void hideLoadingMorePages();

    void restoreFilmsState();

    void hideLoading();

    void setData(List<MovieDTO> films);

    void refreshScrollPosition();

    void showMovieInfoScreen();

    void showNoInternetConnectionView();

    void hideNoInternetConnectionView();
}