package com.moviefinder.ui.screens.actors;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.moviefinder.MovieApp;
import com.moviefinder.R;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ActorsFragment extends Fragment implements ActorsView {

    public static final String TAG = ActorsFragment.class.getName();

    private Unbinder unbinder;
    private ActorsPresenter presenter;

    public static ActorsFragment newInstance() {
        Bundle args = new Bundle();
        ActorsFragment fragment = new ActorsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_actors, container, false);
        unbinder = ButterKnife.bind(this, view);
        presenter = new ActorsPresenterImpl(this, MovieApp.provideMovieRepository());
        return view;
    }
}