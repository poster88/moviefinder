package com.moviefinder.ui.screens.search.view;


import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.moviefinder.data.mapper.MovieDTO;
import com.moviefinder.ui.screens.search.QuerySuggestion;

import java.util.List;


public interface SearchView extends MvpView {

    void fadeDimBackground(int from, int to);

    void clearSuggestions();

    void clearSearchView();

    void hideComponentsContainer();

    void swapSearchSuggestion(List<QuerySuggestion> suggestions);

    void showSearchResults(List<MovieDTO> searchResult);

    void showComponentContainer();

    void restoreSearchResults();

    void showLoadingMorePages();

    void hideLoadingMorePages();

    void setData(List<MovieDTO> searches);

    void showNoResults();

    void showMyPlayListScreen();

    void showSearchInfo();

    void onBackPress();
}
