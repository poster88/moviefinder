package com.moviefinder.ui.screens.tvshow;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.moviefinder.MovieApp;
import com.moviefinder.R;
import com.moviefinder.data.mapper.MovieDTO;
import com.moviefinder.events.BottomNavigationItemSelected;
import com.moviefinder.events.FabEvent;
import com.moviefinder.events.FabSheetEvent;
import com.moviefinder.ui.BaseFragment;
import com.moviefinder.ui.screens.tvshow.view.TVShowsState;
import com.moviefinder.ui.screens.tvshow.view.TVShowsStateImpl;
import com.moviefinder.ui.screens.tvshow.view.TVShowsView;
import com.moviefinder.ui.screens.tvshowinfo.TVShowInfoFragment;
import com.moviefinder.ui.views.EqualSpacingItemDecoration;
import com.moviefinder.utils.AppConstants;
import com.moviefinder.utils.ViewUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class TVShowsFragment extends BaseFragment<TVShowsView, TVShowsPresenter, TVShowsState>
        implements TVShowsView {

    public static final String TAG = "TVShowsFragment";

    public static TVShowsFragment newInstance() {
        Bundle args = new Bundle();
        TVShowsFragment fragment = new TVShowsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.fragment_tv_shows_rv_list) RecyclerView rvTVShows;
    @BindView(R.id.fragment_tv_shows_pb_loading) ProgressBar pbLoading;
    @BindView(R.id.fragment_tv_shows_pb_loading_content) ProgressBar pbLoadingContent;
    @BindView(R.id.fragment_tv_shows_no_internet_connection) LinearLayout layoutNoInternetConnections;

    private Unbinder unbinder;
    private TVShowsAdapter adapter;
    private GridLayoutManager layoutManager;
    private EqualSpacingItemDecoration itemDecoration;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_tv_shows, container, false);
        unbinder = ButterKnife.bind(this, view);
        initComponents();
        setupRecycleView();
        return view;
    }

    private void initComponents() {
        layoutManager = new GridLayoutManager(getContext(), getResources().getInteger(R.integer.columns_count));
        itemDecoration = new EqualSpacingItemDecoration(getResources().getInteger(R.integer.columns_spacing), EqualSpacingItemDecoration.GRID);
        adapter = createAdapter();
        adapter.setItemClickListener((itemId, type) -> presenter.onItemClicked(itemId));
        adapter.setMenuItemClickListener((menuItemId, itemId, position) ->
                presenter.onMenuItemClicked(menuItemId, itemId, position));
    }

    private void setupRecycleView() {
        rvTVShows.addItemDecoration(itemDecoration);
        rvTVShows.setLayoutManager(layoutManager);
        rvTVShows.setAdapter(adapter);
        rvTVShows.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int lastVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
                presenter.onScrolled(visibleItemCount, totalItemCount, lastVisibleItemPosition, dy > 0);
            }
        });
    }

    @Override
    public TVShowsPresenter createPresenter() {
        return new TVShowsPresenterImpl(MovieApp.provideMovieRepository());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @NonNull
    @Override
    public TVShowsState createViewState() {
        return new TVShowsStateImpl();
    }

    @Override
    public void onNewViewStateInstance() {
        presenter.onNewViewStateInstance();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleBottomNavigationEvent(BottomNavigationItemSelected event) {
        presenter.onNavigationItemSelected(event.getEvent());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleFabSheetEvent(FabSheetEvent event){
        presenter.obFabSheetEvent(event.getViewId());
    }

    @OnClick(R.id.fragment_tv_shows_btn_retry)
    public void onRetryBtnClick() {
        viewState.setStateDefault();
        layoutNoInternetConnections.setVisibility(View.GONE);
        presenter.restoreTVShowsState();
    }

    @Override
    public void showTVShowInfoScreen() {
        EventBus.getDefault().post(new FabEvent(false));
        ViewUtils.replaceFragment(Objects.requireNonNull(getFragmentManager()),
                TVShowInfoFragment.newInstance(), getString(R.string.tag_tv_show_info_fragment));
    }

    @Override
    public void showLoading() {
        viewState.setShowLoadingState();
        pbLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        pbLoading.setVisibility(View.GONE);
    }

    @Override
    public void showNoInternetConnectionView() {
        viewState.setStateNoInternetConnections();
        layoutNoInternetConnections.setVisibility(View.VISIBLE);
    }

    @Override
    public void showTVShows(List<MovieDTO> tvShows) {
        viewState.setShowTVShowsState();
        adapter.setData(tvShows);
        rvTVShows.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoadingMorePages() {
        viewState.setShowLoadingMorePagesState();
        pbLoadingContent.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingMorePages() {
        pbLoadingContent.setVisibility(View.GONE);
    }

    @Override
    public void setData(List<MovieDTO> shows) {
        viewState.setShowTVShowsState();
        adapter.addData(shows);
    }

    @Override
    public void refreshScrollPosition() {
        layoutManager.scrollToPosition(AppConstants.DEFAULT_SCROLL_POSITION);
    }

    @Override
    public void hideNoInternetConnectionView() {
        layoutNoInternetConnections.setVisibility(View.GONE);
    }

    @Override
    public void restoreTVShowsState() {
        presenter.restoreTVShowsState();
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        presenter.attachView(this);
    }

    @NonNull
    private TVShowsAdapter createAdapter() {
        TypedValue typedValue = new TypedValue();
        getResources().getValue(R.dimen.rows_count, typedValue, true);
        float rowsCount = typedValue.getFloat();
        int actionBarHeight = Objects.requireNonNull(getActivity()).getTheme()
                .resolveAttribute(R.attr.actionBarSize, typedValue, true)
                ? TypedValue.complexToDimensionPixelSize(typedValue.data, getResources().getDisplayMetrics())
                : 0;
        int imageHeight = (int) ((getResources().getDisplayMetrics().heightPixels - actionBarHeight
                - ViewUtils.dpToPx(getResources().getInteger(R.integer.image_height))) / rowsCount);
        int columns = getResources().getInteger(R.integer.columns_count);
        int imageWidth = (getResources().getDisplayMetrics().widthPixels
                - ViewUtils.dpToPx(getResources().getInteger(R.integer.image_width))) / columns;
        return new TVShowsAdapter(imageHeight, imageWidth);
    }
}