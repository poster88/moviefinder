package com.moviefinder.ui.screens.search;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.moviefinder.R;
import com.moviefinder.data.mapper.MovieDTO;
import com.moviefinder.ui.views.OnItemClickListener;
import com.moviefinder.utils.ViewUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String TAG = "SearchAdapter";

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    private List<MovieDTO> data = new ArrayList<>();
    private OnItemClickListener onItemClickListener;
    private OnMenuItemClickListener onMenuItemClickListener;
    private PopupMenu popupMenu;

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder rh;
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search_result, parent, false);
            rh = new SearchViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            rh = new LoadingViewHolder(view);
        }
        return rh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MovieDTO model = data.get(position);
        if (holder instanceof SearchViewHolder) {
            SearchViewHolder h = (SearchViewHolder) holder;
            ViewUtils.loadImage(h.imgPoster, model.getPosterPath(), R.drawable.ic_square_placeholder_light_gray);
            h.textTitle.setText(model.getTitle());
            h.rbMovieRating.setRating(model.getFormattedVoteAverage());
            h.textGenre.setText(model.getStringGenres());
            h.textReleaseDate.setText(model.getFormattedDate());
            h.itemView.setOnClickListener(v ->
                    onItemClickListener.onItemClicked(model.getId(), model.getMediaType()));
            h.imgMenu.setOnClickListener(v -> {
                popupMenu = new PopupMenu(v.getContext(), v);
                popupMenu.inflate(R.menu.menu_context_movie);
                popupMenu.show();
                popupMenu.setOnMenuItemClickListener(item -> {
                    onMenuItemClickListener.onMenuItemClick(item.getItemId(), model.getId(), model.getMediaType());
                    return true;
                });
            });
        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder h = (LoadingViewHolder) holder;
            h.loading.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return null == data.get(position) ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    public void setData(List<MovieDTO> list) {
        data.clear();
        data.addAll(list);
        notifyDataSetChanged();
    }

    public void addAll(List<MovieDTO> list) {
        for (MovieDTO m : list) {
            add(m);
        }

    }

    public void remove(int i) {
        int pos = i;
        if (pos > -1) {
            data.remove(pos);
            notifyItemRemoved(pos);
        }
    }

    public void add(MovieDTO item) {
        data.add(item);
        notifyItemInserted(data.size() - 1);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setOnMenuItemClickListener(OnMenuItemClickListener onMenuClickListener) {
        this.onMenuItemClickListener = onMenuClickListener;
    }

    public interface OnMenuItemClickListener {
        void onMenuItemClick(int menuItemId, int itemId, String type);
    }

    class SearchViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_search_img_poster) ImageView imgPoster;
        @BindView(R.id.item_search_text_title) TextView textTitle;
        @BindView(R.id.item_search_text_genre) TextView textGenre;
        @BindView(R.id.item_search_rb_rating) RatingBar rbMovieRating;
        @BindView(R.id.item_search_text_release_date) TextView textReleaseDate;
        @BindView(R.id.item_search_img_btn_menu) ImageView imgMenu;

        SearchViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class LoadingViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_loading_progress_bar) ProgressBar loading;

        LoadingViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}