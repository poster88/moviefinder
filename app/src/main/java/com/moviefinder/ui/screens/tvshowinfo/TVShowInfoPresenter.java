package com.moviefinder.ui.screens.tvshowinfo;


import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.moviefinder.ui.screens.tvshowinfo.view.TVShowInfoView;

public interface TVShowInfoPresenter extends MvpPresenter<TVShowInfoView> {

    void onNavigationBtnClick();

    void onNewViewStateInstance();

    void restoreMovieInfoState();
}
