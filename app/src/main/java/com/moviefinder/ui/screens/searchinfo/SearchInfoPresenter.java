package com.moviefinder.ui.screens.searchinfo;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.moviefinder.ui.screens.searchinfo.view.SearchInfoView;

public interface SearchInfoPresenter extends MvpPresenter<SearchInfoView> {

    void onNavigationBtnClick();

    void onNewViewStateInstance();

    void restoreSearchInfoState();
}