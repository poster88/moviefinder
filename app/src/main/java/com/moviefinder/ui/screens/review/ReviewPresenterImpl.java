package com.moviefinder.ui.screens.review;

import android.util.Log;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.moviefinder.R;
import com.moviefinder.data.repository.MovieRepository;
import com.moviefinder.events.MessageEvent;
import com.moviefinder.ui.screens.review.view.ReviewView;
import com.moviefinder.utils.AppConstants;
import com.moviefinder.utils.ServerException;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.moviefinder.utils.AppConstants.DEFAULT_PAGE;
import static com.moviefinder.utils.AppConstants.RESPONSE_ITEM_COUNT;


public class ReviewPresenterImpl extends MvpBasePresenter<ReviewView> implements ReviewPresenter {

    private static final String TAG = "ReviewPresenterImpl";

    private MovieRepository repository;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private int moviesPage = DEFAULT_PAGE;
    private int tvShowsPage = DEFAULT_PAGE;
    private int categoryId = R.id.action_actual;
    private int filterGenreId = R.id.fab_sheet_genre_all;

    public ReviewPresenterImpl(MovieRepository repository) {
        this.repository = repository;
    }

    @Override
    public void onNewViewStateInstance() {
        compositeDisposable.add(getMovies());
        compositeDisposable.add(getTVShows());
    }

    private Disposable getTVShows() {
        return repository.getTVShows(tvShowsPage, categoryId, filterGenreId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(movies -> movies.subList(0, RESPONSE_ITEM_COUNT - 1))
                .doOnSubscribe(disposable -> ifViewAttached(ReviewView::showTVShowsLoading))
                .doAfterTerminate(() -> ifViewAttached(ReviewView::hideTVShowLoading))
                .onErrorReturn(throwable -> new ArrayList<>())
                .subscribe(tvShows -> ifViewAttached(view -> view.showTVShows(tvShows)),
                        throwable -> Log.d(TAG, "getTVShows error: " + throwable.fillInStackTrace()));
    }

    private Disposable getMovies() {
        return repository.getFilms(moviesPage, categoryId, filterGenreId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(movies -> movies.subList(0, RESPONSE_ITEM_COUNT - 1))
                .doOnSubscribe(disposable -> ifViewAttached(ReviewView::showMoviesLoading))
                .doAfterTerminate(() -> ifViewAttached(ReviewView::hideMoviesLoading))
                .onErrorReturn(throwable -> {
                    handleError(throwable);
                    return new ArrayList<>();
                })
                .subscribe(movies -> ifViewAttached(view -> view.showMovies(movies)),
                        throwable -> Log.d(TAG, "getMovies error: " + throwable.fillInStackTrace()));
    }

    private void handleError(Throwable throwable) {
        ServerException error = (ServerException) throwable;
        int errorId = R.string.error_default;
        if (error.getKind() == ServerException.Kind.NETWORK) {
            errorId = R.string.error_no_connection;
        } else if (error.getKind() == ServerException.Kind.HTTP) {
            errorId = R.string.error_bad_http_request;
        } else if (error.getKind() == ServerException.Kind.UNEXPECTED) {
            errorId = R.string.error_unexpected;
        }
        int finalErrorId = errorId;
        ifViewAttached(view -> view.showError(finalErrorId));
    }

    @Override
    public void restoreMovies() {
        compositeDisposable.add(getMovies());
    }

    @Override
    public void restoreTVShows() {
        compositeDisposable.add(getTVShows());
    }

    @Override
    public void handleConnectionEvent(boolean connection) {
        if (connection) {
            ifViewAttached(view -> {
                view.restoreMoviesState();
                view.restoreTVShowsState();
            });
        }
    }

    @Override
    public void onTVShowItemClicked(int itemId, String type) {
        repository.setLastTVShowDetails(categoryId, itemId);
        ifViewAttached(ReviewView::showTVShowsScreen);
    }

    @Override
    public void onMovieItemClicked(int itemId, String type) {
        repository.setLastMovieDetails(categoryId, itemId);
        ifViewAttached(ReviewView::showMovieInfoScreen);
    }

    @Override
    public void onMovieMenuItemClicked(int menuItemId, int itemId, int position) {
        if (menuItemId == R.id.action_add_to_favorite) {
            repository.setLastMovieDetails(categoryId, itemId);
            compositeDisposable.add(addToItemToFavorite(String.valueOf(itemId), AppConstants.TYPE_MOVIE));
        }
    }

    @Override
    public void onTVShowMenuItemClicked(int menuItemId, int itemId, int position) {
        if (menuItemId == R.id.action_add_to_favorite) {
            repository.setLastTVShowDetails(categoryId, itemId);
            compositeDisposable.add(addToItemToFavorite(String.valueOf(itemId), AppConstants.TYPE_TV_SHOW));
        }
    }

    private Disposable addToItemToFavorite(String itemId, String type) {
        return repository.addToFavorites(itemId, type)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(isItemAdded -> {
                    if (isItemAdded) {
                        EventBus.getDefault().post(new MessageEvent(R.string.add_to_favorite));
                    } else {
                        EventBus.getDefault().post(new MessageEvent(R.string.fail_add_to_favorite));
                    }
                });
    }
}