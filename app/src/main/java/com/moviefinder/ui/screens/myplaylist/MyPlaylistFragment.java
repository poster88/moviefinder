package com.moviefinder.ui.screens.myplaylist;


import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.moviefinder.MovieApp;
import com.moviefinder.R;
import com.moviefinder.data.mapper.MovieDTO;
import com.moviefinder.ui.BaseFragment;
import com.moviefinder.ui.screens.movieinfo.MovieInfoFragment;
import com.moviefinder.ui.screens.myplaylist.view.MyPlaylistView;
import com.moviefinder.ui.screens.myplaylist.view.MyPlaylistViewState;
import com.moviefinder.ui.screens.myplaylist.view.MyPlaylistViewStateImpl;
import com.moviefinder.ui.screens.tvshowinfo.TVShowInfoFragment;
import com.moviefinder.ui.views.ItemOffsetDecoration;
import com.moviefinder.utils.ViewUtils;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MyPlaylistFragment extends BaseFragment<MyPlaylistView, MyPlaylistPresenter, MyPlaylistViewState>
        implements MyPlaylistView {

    public static final String TAG = "MyPlaylistFragment";
    public static MyPlaylistFragment newInstance() {
        Bundle args = new Bundle();
        MyPlaylistFragment fragment = new MyPlaylistFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.fragment_my_playlist_toolbar) Toolbar toolbar;
    @BindView(R.id.fragment_my_playlist_rv_playlist) RecyclerView rvItemPlaylist;
    @BindView(R.id.fragment_my_playlist_no_items_container) LinearLayout layoutNoItemsContainer;

    private Unbinder unbinder;
    private MyPlayListAdapter adapter;
    private LinearLayoutManager layoutManager;
    private ItemOffsetDecoration itemDecoration;

    @Override
    public MyPlaylistPresenter createPresenter() {
        return new MyPlaylistPresenterImpl(MovieApp.provideMovieRepository());
    }

    @NonNull
    @Override
    public MyPlaylistViewState createViewState() {
        return new MyPlaylistViewStateImpl();
    }

    @Override
    public void onNewViewStateInstance() {
        presenter.onNewViewStateInstance();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_my_playlist, container, false);
        unbinder = ButterKnife.bind(this, view);
        setupToolbar();
        initComponents();
        setupRecycleView();
        return view;
    }

    private void setupRecycleView() {
        rvItemPlaylist.addItemDecoration(itemDecoration);
        rvItemPlaylist.setLayoutManager(layoutManager);
        rvItemPlaylist.setAdapter(adapter);
    }

    private void initComponents() {
        itemDecoration = new ItemOffsetDecoration(Objects.requireNonNull(getContext()),
                R.dimen.item_rv_offset, ItemOffsetDecoration.ORIENTATION_VERTICAL);
        layoutManager = new LinearLayoutManager(getContext());
        adapter = new MyPlayListAdapter();
        adapter.setOnItemClickListener((itemId, type) -> presenter.onItemClicked(itemId, type));
        adapter.setOnMenuItemClickListener((menuItemId, itemId, position) ->
                presenter.onMenuItemClicked(menuItemId, itemId, position));
    }

    private void setupToolbar() {
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.inflateMenu(R.menu.menu_my_playlist);
        toolbar.setTitle(R.string.tag_play_list_title);
        toolbar.setNavigationOnClickListener(v -> presenter.onNavigationBtnClick());
        toolbar.setOnMenuItemClickListener(item -> {
            presenter.onToolBarMenuItemClick(item.getItemId(), adapter.getItemCount());
            return true;
        });
    }

    @OnClick(R.id.fragment_my_playlist_btn_go_main)
    public void onBtnGoMainClick(View v) {
        presenter.onBtnGoMainClick(v.getId());
    }

    @Override
    public void showAlertDialog(int titleId, int icon, boolean isCancelable, int posBtnTitleId, int negBtnTitleId) {
        AlertDialog ab = new AlertDialog.Builder(getContext())
                .setTitle(titleId)
                .setIcon(icon)
                .setCancelable(isCancelable)
                .setPositiveButton(posBtnTitleId, (dialog, which) -> presenter.onAlertPositiveBtnClick())
                .setNegativeButton(negBtnTitleId, (dialog, which) -> dialog.dismiss())
                .create();

        ab.show();
    }

    @Override
    public void showPlaylist(List<MovieDTO> list) {
        viewState.showPlaylistState();
        layoutNoItemsContainer.setVisibility(View.GONE);
        adapter.setData(list);
    }

    @Override
    public void showNoResultView() {
        viewState.showNoResultState();
        layoutNoItemsContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void restoreListState() {
        presenter.restoreList();
    }

    @Override
    public void removeFragment() {
        Objects.requireNonNull(getActivity()).onBackPressed();
    }

    @Override
    public void updateAdapter(int position) {
        adapter.removeItem(position);
    }

    @Override
    public void updateAdapter() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void removeAllItems() {
        adapter.removeAll();
    }

    @Override
    public void showMovieInfoScreen() {
        ViewUtils.replaceFragment(Objects.requireNonNull(getFragmentManager()),
                MovieInfoFragment.newInstance(), getString(R.string.tag_movie_info));
    }

    @Override
    public void showTVShowsScreen() {
        ViewUtils.replaceFragment(Objects.requireNonNull(getFragmentManager()),
                TVShowInfoFragment.newInstance(), getString(R.string.tag_tv_show_info_fragment));
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        presenter.attachView(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}