package com.moviefinder.ui;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.hannesdorfmann.mosby3.mvp.viewstate.MvpViewStateFragment;
import com.hannesdorfmann.mosby3.mvp.viewstate.ViewState;

public abstract class BaseFragment<V extends MvpView, P extends MvpPresenter<V>, VS extends ViewState<V>>
        extends MvpViewStateFragment<V, P, VS> {

}
